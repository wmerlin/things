Bits and bobs and things I've done.

    sgamesite/

            Django webapp. Notable as my first foray into the field and for how
            it handles an inherent gameplay problem.

    pyircd/

            What the name implies: a fairly usable IRCd implementation which
            happens to be implemented in Python w/ Twisted. Highly abusable
            thanks to Twisted Manhole.

    misc/

            Toys, one-offs and other curiosities.
