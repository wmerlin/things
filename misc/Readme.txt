fizzbuzz.py

    The obvious thing, implemented in non-obvious manner, because it was more
    fun that way.

mandelbrot.py

    $ python3 mandelbrot.py 20 10

pyircd.py

    A single-file, mostly functional ~900 line zero-configuration IRCd.

blake2/

    Reference and AVX2 implementations of the BLAKE2b hash function.

    Oddly enough, the AVX version is actually *slower* than the straightforward
    C version, but I lack the depth of knowledge necessary to know why.
