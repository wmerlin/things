#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <QtCore/QtGlobal>

#include "blake2.hpp"

/*
 * BLAKE2b.
 *
 * This is a fairly straightforward reference implementation. It performs
 * decently enough for anything Rh2 will need it for.
 */

static const uint64_t IV[8] = {
    0x6a09e667f3bcc908, 0xbb67ae8584caa73b,
    0x3c6ef372fe94f82b, 0xa54ff53a5f1d36f1,
    0x510e527fade682d1, 0x9b05688c2b3e6c1f,
    0x1f83d9abfb41bd6b, 0x5be0cd19137e2179
};

static const int SIGMA[12][16] = {
    { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15},
    {14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3},
    {11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4},
    { 7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8},
    { 9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13},
    { 2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9},
    {12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11},
    {13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10},
    { 6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5},
    {10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13,  0},
    // Extend by two rows to avoid modulus
    { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15},
    {14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3}
};

static inline uint64_t rotr64(uint64_t v, int d) {
    return (v >> d) | (v << (64 - d));
}

BLAKE2b::Params::Params() {
    this->fields.digest_length = 64; // 512-bit hash
    this->fields.key_length = 0;     // Assume no key.

    // Not tree hashing.
    this->fields.fanout = 1;
    this->fields.depth = 1;
    this->fields.leaf_length = 0;
    this->fields.node_offset = 0;
    this->fields.node_depth = 0;
    this->fields.inner_length = 0;

    // Assume no salt or personalization string.
    memset(this->fields._reserved, 0, sizeof(this->fields._reserved));
    memset(this->fields.salt, 0, sizeof(this->fields.salt));
    memset(this->fields.personalization, 0, sizeof(this->fields.personalization));
}

BLAKE2b::BLAKE2b(Params* params) {
    if(params) {
        init_state(params);
    } else {
        Params p;
        init_state(&p);
    }
}

BLAKE2b::~BLAKE2b() {
    // TODO: Clear memory?
}

void BLAKE2b::update(const uint8_t* data, size_t length) {
    // It's important that we never completely empty our data buffer, because
    // the last data block must be treated specially. So our general algorithm
    // here is:
    //
    // 1) So long as data is still available:
    // 1a) Fill our buffer
    // 1b) If it's full, and there is more available, hash
    // 1c) If it isn't full, end
    while(length) {
        // Try to fill the internal buffer. Copy either enough to fill it, or
        // whatever is left here. (Note: this might be zero, if we currently
        // have a full block, but were saving it for later.)
        int copy = qMin(sizeof(this->buffer) - this->buffer_length, length);
        memcpy(this->buffer + this->buffer_length, data, copy);
        this->buffer_length += copy;
        data += copy;
        length -= copy;

        // If there's more data to be had, we can hash this block. Otherwise
        // we keep it for later, just in case this is the last block.
        if(length) {
            // Increment counter, potentially handling overflow (unlikely, but
            // why not).
            this->t[0] += BLOCK_BYTES;
            if(this->t[0] < BLOCK_BYTES) {
                this->t[1] += 1;
            }

            compress((uint64_t*)this->buffer);

            this->buffer_length = 0;
        } else {
            // No more data available means we must have a partial block now.
            // Save it for later.
            break;
        }
    }
}

void BLAKE2b::finalize(uint8_t hash[HASH_LENGTH]) {
    // Finish off the last block. Pad with zeros:
    for(size_t i = this->buffer_length; i < sizeof(this->buffer); i++) {
        this->buffer[i] = 0;
    }

    // Increment counter.
    this->t[0] += this->buffer_length;
    if(this->t[0] < this->buffer_length) {
        this->t[1] += 1;
    }

    // Set finalization flag.
    this->f[0] = 0xffffffffffffffff;

    compress((uint64_t*)this->buffer);

    memcpy(hash, this->h, HASH_LENGTH);
}

void BLAKE2b::hash(const uint8_t* data, size_t length, uint8_t hash[HASH_LENGTH]) {
    BLAKE2b blake2b;
    blake2b.update(data, length);
    blake2b.finalize(hash);
}

void BLAKE2b::init_state(Params* params) {
    for(int i = 0; i < 8; i++) {
        this->h[i] = params->words[i] ^ IV[i];
    }

    this->t[0] = 0;
    this->t[1] = 0;

    this->f[0] = 0;
    this->f[1] = 0;

    memset(this->buffer, 0, sizeof(this->buffer));
    this->buffer_length = 0;
}

#define G(r,i,a,b,c,d) \
    a = a + b + m[SIGMA[r][2*i]]; \
    d = rotr64(d ^ a, 32); \
    c = c + d; \
    b = rotr64(b ^ c, 24); \
    a = a + b + m[SIGMA[r][2*i + 1]]; \
    d = rotr64(d ^ a, 16); \
    c = c + d; \
    b = rotr64(b ^ c, 63);

#define ROUND(r) \
    G(r, 0, v[0], v[4],  v[8], v[12]); \
    G(r, 1, v[1], v[5],  v[9], v[13]); \
    G(r, 2, v[2], v[6], v[10], v[14]); \
    G(r, 3, v[3], v[7], v[11], v[15]); \
    G(r, 4, v[0], v[5], v[10], v[15]); \
    G(r, 5, v[1], v[6], v[11], v[12]); \
    G(r, 6, v[2], v[7],  v[8], v[13]); \
    G(r, 7, v[3], v[4],  v[9], v[14]);

void BLAKE2b::compress(const uint64_t m[BLAKE2b::BLOCK_WORDS]) {
    uint64_t v[16] = {
        this->h[0], this->h[1], this->h[2], this->h[3],

        this->h[4], this->h[5], this->h[6], this->h[7],

        IV[0],
        IV[1],
        IV[2],
        IV[3],

        IV[4] ^ this->t[0],
        IV[5] ^ this->t[1],
        IV[6] ^ this->f[0],
        IV[7] ^ this->f[1]
    };

    ROUND(0);
    ROUND(1);
    ROUND(2);
    ROUND(3);
    ROUND(4);
    ROUND(5);
    ROUND(6);
    ROUND(7);
    ROUND(8);
    ROUND(9);
    ROUND(10);
    ROUND(11);

    this->h[0] ^= v[0] ^ v[ 8];
    this->h[1] ^= v[1] ^ v[ 9];
    this->h[2] ^= v[2] ^ v[10];
    this->h[3] ^= v[3] ^ v[11];
    this->h[4] ^= v[4] ^ v[12];
    this->h[5] ^= v[5] ^ v[13];
    this->h[6] ^= v[6] ^ v[14];
    this->h[7] ^= v[7] ^ v[15];
}
