#ifndef BLAKE2_HPP
#define BLAKE2_HPP

#include <stddef.h>
#include <stdint.h>

class BLAKE2b {
public:
    union Params {
        Params();

        struct {
            uint8_t digest_length;       // Output digest length in bytes. Value in [1, 64]
            uint8_t key_length;          // Key length in bytes. Value in [0, 64]
            uint8_t fanout;        // Tree hashing. Any value. 0=unlimited, 1=sequential
            uint8_t depth;         // Tree hashing. Value in [1, 255]. 255=unlimited, 1=sequential
            uint32_t leaf_length;  // Tree hashing. Any value. 0=unlimited or sequential.
            uint64_t node_offset;  // Tree hashing. Any value. 0=first, leftmost, leaf, or sequential.
            uint8_t node_depth;    // Tree hashing. Any value. 0=leaf or sequential.
            uint8_t inner_length;  // Tree hashing. Value in [0, 64]. 0=sequential.
            uint8_t _reserved[14];       // Reserved. Set to zero.
            uint8_t salt[16];            // Arbitrary salt. All zero by default.
            uint8_t personalization[16]; // Arbitrary personalization. All zero by default.
        } fields;

        uint8_t bytes[64];
        uint64_t words[8];
    };
    static_assert(sizeof(Params) == 64, "BLAKE2b::Params must be exactly 64 bytes");

    static const unsigned int HASH_LENGTH = 64;
    static const unsigned int BLOCK_WORDS = 16;
    static const unsigned int BLOCK_BYTES = 128;

    BLAKE2b(Params* params=NULL);
    ~BLAKE2b();

    void update(const uint8_t* data, size_t length);
    void finalize(uint8_t hash[HASH_LENGTH]);

    static void hash(const uint8_t* data, size_t length, uint8_t hash[HASH_LENGTH]);

private:
    void init_state(Params* params);
    void compress(const uint64_t m[BLAKE2b::BLOCK_WORDS]);

    // Hash state
    uint64_t h[8];
    uint64_t t[2];
    uint64_t f[2];

    uint8_t buffer[BLOCK_BYTES];
    size_t buffer_length;
};

#endif
