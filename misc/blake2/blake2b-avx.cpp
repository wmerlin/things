#include <immintrin.h>

#include "blake2.hpp"
#include "blake2b-internal.hpp"

/*
 * AVX2 implementation of BLAKE2b.
 */

static const uint64_t BLAKE2B_SIGMA_AVX[12][16] = {
    { 6,  4,  2,  0,  7,  5,  3,  1, 14, 12, 10,  8, 15, 13, 11,  9},
    {13,  9,  4, 14,  6, 15,  8, 10,  5, 11,  0,  1,  3,  7,  2, 12},
    {15,  5, 12, 11, 13,  2,  0,  8,  9,  7,  3, 10,  4,  1,  6, 14},
    {11, 13,  3,  7, 14, 12,  1,  9, 15,  4,  5,  2,  8,  0, 10,  6},
    {10,  2,  5,  9, 15,  4,  7,  0,  3,  6, 11, 14, 13,  8, 12,  1},
    { 8,  0,  6,  2,  3, 11, 10, 12,  1, 15,  7,  4,  9, 14,  5, 13},
    { 4, 14,  1, 12, 10, 13, 15,  5,  8,  9,  6,  0, 11,  2,  3,  7},
    { 3, 12,  7, 13,  9,  1, 14, 11,  2,  8, 15,  5, 10,  6,  4,  0},
    { 0, 11, 14,  6,  8,  3,  9, 15, 10,  1, 13, 12,  5,  4,  7,  2},
    { 1,  7,  8, 10,  5,  6,  4,  2, 13,  3,  9, 15,  0, 12, 14, 11},
    { 6,  4,  2,  0,  7,  5,  3,  1, 14, 12, 10,  8, 15, 13, 11,  9},
    {13,  9,  4, 14,  6, 15,  8, 10,  5, 11,  0,  1,  3,  7,  2, 12}
};

#if defined(__GNUC__) && !defined(__clang__)
 #define GATHER(m,v,s) _mm256_i64gather_epi64((const long long int*)m, v, s);
#else
 #define GATHER(m,v,s) _mm256_i64gather_epi64((int*)m, v, s);
#endif

#define ROTR_256(v,d) \
    tmp = _mm256_srli_epi64(v, d); \
    v = _mm256_slli_epi64(v, 64 - d); \
    v = _mm256_or_si256(v, tmp);

#define G(r,i,a,b,c,d) \
    sigma = GATHER(m, *(__m256i*)&BLAKE2B_SIGMA_AVX[r][i], 8); \
    a = _mm256_add_epi64(a, b); \
    a = _mm256_add_epi64(a, sigma); \
    d = _mm256_xor_si256(d, a); \
    ROTR_256(d, 32); \
    c = _mm256_add_epi64(c, d); \
    b = _mm256_xor_si256(b, c); \
    ROTR_256(b, 24); \
    sigma = GATHER(m, *(__m256i*)&BLAKE2B_SIGMA_AVX[r][i + 4], 8); \
    a = _mm256_add_epi64(a, b); \
    a = _mm256_add_epi64(a, sigma); \
    d = _mm256_xor_si256(d, a); \
    ROTR_256(d, 16); \
    c = _mm256_add_epi64(c, d); \
    b = _mm256_xor_si256(b, c); \
    ROTR_256(b, 63);

#define ROUND(r) \
    G(r, 0, a, b, c, d); \
    b = _mm256_permute4x64_epi64(b, _MM_SHUFFLE(0, 3, 2, 1)); \
    c = _mm256_permute4x64_epi64(c, _MM_SHUFFLE(1, 0, 3, 2)); \
    d = _mm256_permute4x64_epi64(d, _MM_SHUFFLE(2, 1, 0, 3)); \
    G(r, 8, a, b, c, d); \
    b = _mm256_permute4x64_epi64(b, _MM_SHUFFLE(2, 1, 0, 3)); \
    c = _mm256_permute4x64_epi64(c, _MM_SHUFFLE(1, 0, 3, 2)); \
    d = _mm256_permute4x64_epi64(d, _MM_SHUFFLE(0, 3, 2, 1));

extern "C" void blake2b_compress(BLAKE2b::State* state, const uint64_t m[BLAKE2b::BLOCK_WORDS]) {
    // Note: The compiler generates aligned instructions by default both here
    // and lower down, so we force the use of unaligned instructions.

    __m256i a = _mm256_loadu_si256((__m256i*)&state->h[0]);
    __m256i b = _mm256_loadu_si256((__m256i*)&state->h[4]);
    __m256i c = _mm256_loadu_si256((__m256i*)&BLAKE2B_IV[0]);
    __m256i d = _mm256_xor_si256(_mm256_loadu_si256((__m256i*)&BLAKE2B_IV[4]),
                                 _mm256_loadu_si256((__m256i*)&state->t[0]));
    __m256i sigma;
    __m256i tmp;

    ROUND(0);
    ROUND(1);
    ROUND(2);
    ROUND(3);
    ROUND(4);
    ROUND(5);
    ROUND(6);
    ROUND(7);
    ROUND(8);
    ROUND(9);
    ROUND(10);
    ROUND(11);

    _mm256_storeu_si256((__m256i*)&state->h[0],
                        _mm256_xor_si256(*(__m256i*)&state->h[0],
                        _mm256_xor_si256(a, c)));
    _mm256_storeu_si256((__m256i*)&state->h[4],
                        _mm256_xor_si256(*(__m256i*)&state->h[4],
                        _mm256_xor_si256(b, d)));
}
