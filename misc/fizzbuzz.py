#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def execute(p):
    stack = []
    ip = 0
    while ip < len(p):
        inst = p[ip]
        #print("[%03s %s] %r" % (ip, inst, stack))
        if inst in "0123456789":
            stack.append(int(inst))
            ip += 1
        elif inst == "i":
            n = stack.pop()
            stack.append(int(p[ip+1:ip+1+n]))
            ip += 1 + n
        elif inst == "s":
            n = stack.pop()
            stack.append(p[ip+1:ip+1+n])
            ip += 1 + n
        elif inst == "d":
            stack.append(stack[-1])
            ip += 1
        elif inst == "u":
            i = stack.pop()
            stack.append(stack[-i])
            ip += 1
        elif inst == "%":
            y, x = stack.pop(), stack.pop()
            stack.append(x % y)
            ip += 1
        elif inst == "?":
            target = stack.pop()
            b = stack.pop()
            if not b:
                ip = target
            else:
                ip += 1
        elif inst == " ":
            ip += 1
        elif inst == "p":
            s = stack.pop()
            print(s, end="")
            ip += 1
        elif inst == "q":
            break
        elif inst == "!":
            stack[-1] = int(not stack[-1])
            ip += 1
        elif inst == "&":
            a, b = stack.pop(), stack.pop()
            stack.append(int(a and b))
            ip += 1
        elif inst == "-":
            stack.pop()
            ip += 1
        elif inst == "j":
            ip = stack.pop()
        elif inst == "=":
            stack.append(int(stack.pop() == stack.pop()))
            ip += 1
        elif inst == "+":
            stack[-1] += 1
            ip += 1
        else:
            raise ValueError("unknown opcode", inst)

                           # 3&5?                   v  5?              v  3?             v
                           #                      >                  >                 >    v              !done?    v
program = "0 d3%!2u5%!2u2u& 2i41?8sFizzBuzzp--2i81j 2i60?4sBuzzp-2i81j 2i78?4sFizzp2i81j dp 1s\np d3i100=! 3i105?+2j -"
execute(program)
