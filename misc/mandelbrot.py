#!/usr/bin/env python3
# -*- coding: utf8 -*-

import sys

scalex, scaley = int(sys.argv[1]), int(sys.argv[2])

Iterations = 10

def iter(z, c):
    return z**2 + c

def ok(x, y):
    z = 0
    c = complex(x, y)
    for t in range(Iterations):
        if abs(z) > 2:
            return False
        z = iter(z, c)
    return True

row = -2
while row < 2:
    s = ""
    col = -2
    while col < 2:
        if ok(col, row):
            s += "**"
        else:
            s += "  "
        col += 1/scalex
    print(s)
    row += 1/scaley
