#!/usr/bin/env python
# -*- coding: utf-8 -*-
################################################################################
##                     Copyright (C) 2009, Wade Sowders                       ##
##                        This file is part of PyIRCD.                        ##
##                                                                            ##
##  PyIRCD is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCD is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for  ##
##  more details.                                                             ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCD. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from twisted.internet import reactor
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineOnlyReceiver
from twisted.python.util import InsensitiveDict
import re
import string
import time

ServerPrefix = "irc.somewhere.net"
ServerInfo = "AnIRCnetwork"

PyIrcdVersion = "PyIRCD1.0"
CreationTimestamp = "<never>"
AvailUserModes = "i"
AvailChannelModes = "ntbemiIs"

################################################################################
#
#                 Things remaining to be implemented:
#
# General:
#
#     -- A more robust parser. It is currently possible to crash the server by
#        sending properly malformed commands.
#
#     -- On a related note, it should check more things for invalidity,
#        particularly legal characters in a nickname or channel name.
#
#     -- Configurability! Everything is hard coded.
#
#     -- This whole thing is a gigantic mess. Most of it is one function.
#
# Commands from the RFC (not including things involving networking):
#
#     User commands:
#         WHOWAS   -- Obtain information about a previously known user.
#         USERHOST -- User information: Whether user is an operator, or away.
#
#     Channel commands:
#         LIST     -- List channels and their topics.
#
#     Server commands:
#         STATS    -- Query particular server statistics.
#         ADMIN    -- Get adminstrator information.
#
#     Operator commands:
#         OPER     -- Obtain operator privileges.
#         KILL     -- Forcibly drop a client connection.
#
# Channel modes:
#
#         +k       -- Channel key [param]
#         +l       -- User limit [param]
#
################################################################################

_NonEscaped = string.ascii_lowercase + string.ascii_uppercase + string.digits
def Mask(mask):
    # More or less stolen from re.
    chars = list(mask)
    for i, char in enumerate(mask):
        if char == "*":
            chars[i] = ".*"
        elif char not in _NonEscaped:
            chars[i] = "\\" + char
    return re.compile("".join(chars), re.IGNORECASE)

class Channel:
    def __init__(self, server, name):
        self.server = server

        self.name = name
        self.topic = None

        # Channel modes
        self.no_msgs_from_outside = False # +n
        self.op_only_topic = False        # +t
        self.moderated = False            # +m
        self.secret = False               # +s

        self.invite_only = False          # +i
        self.invited = [] # List of Protocol objects
        self.invite_ex = []               # +I
        self._invite_ex = []

        # banlist is a list of masks, _banlist of re.Pattern objects.
        # Todo: Find a better system.
        self.banlist = []                 # +b
        self._banlist = []

        self.banexlist = []               # +e
        self._banexlist = []

        self.users = InsensitiveDict()
        self.chanops = InsensitiveDict()
        self.voiced = InsensitiveDict()

    def SendNames(self, user):
        # Twisted's InsensitiveDict seems broken.
        #
        #   File "/usr/lib/python2.6/site-packages/twisted/python/util.py", line 50, in __getitem__
        #     return self.data[k][1]
        #   exceptions.KeyError: 0
        #
        # Using self.users.keys() instead of self.users seems to work.
        is_on = user.nickname in self.users
        if not is_on and self.secret:
            return user.ErrorCode("366", self.name, "End of /NAMES list")
        acc = ""
        for nickname, t_user in self.users.items():
            if t_user.is_invisible and not is_on:
                continue
            if nickname in self.chanops: acc += "@"
            elif nickname in self.voiced: acc += "+"
            acc += nickname + " "
        if acc:
            user.ErrorCode("353", "=", self.name, acc)
        user.ErrorCode("366", self.name, "End of /NAMES list")

    def JoinUser(self, user):
        self.users[user.nickname] = user

        for t_user in self.users.values():
            t_user.SendFrom(user, "JOIN", self.name)

        if self.topic:
            self.SendTopic(user)
        self.SendNames(user)

        if user in self.invited: self.invited.remove(user)

    def JoinInitialUser(self, user):
        self.users[user.nickname] = user
        self.chanops[user.nickname] = user

        user.SendFrom(user, "JOIN", self.name)
        # Never send topic.
        self.SendNames(user)

        if user in self.invited: self.invited.remove(user)

    def SendTopic(self, user):
        if self.topic is None:
            # Note: For some reason this is only used until somebody sets a
            # topic for the first time. After that, the topic is unset (blank,
            # or "") and the above is used.
            user.ErrorCode("331", self.name, "No topic is set.")
        else:
            user.ErrorCode("332", self.name, self.topic)
            # Todo: Send timestamp.

    def SetTopic(self, by, to):
        self.topic = to
        for user in self.users.values():
            user.SendFrom(by, "TOPIC", self.name, to)

    def _RemoveUser(self, user):
        nickname = user.nickname
        del self.users[nickname]
        if nickname in self.chanops: del self.chanops[nickname]
        if nickname in self.voiced: del self.voiced[nickname]
        if user in self.invited: self.invited.remove(user)

    def _DeleteIfEmpty(self):
        if not self.users:
            del self.server.channels[self.name]

    def NotifyPart(self, user, message):
        for t_user in self.users.values():
            if message:
                t_user.SendFrom(user, "PART", self.name, message)
            else:
                t_user.SendFrom(user, "PART", self.name)

        self._RemoveUser(user)
        self._DeleteIfEmpty()

    def NotifyQuit(self, user, message):
        self._RemoveUser(user)

        for t_user in self.users.values():
            if message:
                t_user.SendFrom(user, "QUIT", message)
            else:
                t_user.SendFrom(user, "QUIT")

        self._DeleteIfEmpty()

    def SendMessage(self, user, message):
        for t_user in self.users.values():
            if t_user is not user:
                t_user.SendFrom(user, "PRIVMSG", self.name, message)

    def NotifyKick(self, by, user, comment):
        for t_user in self.users.values():
            t_user.SendFrom(by, "KICK", self.name, user.nickname, comment)
        self._RemoveUser(user)
        self._DeleteIfEmpty()

    def NotifyNickChange(self, user, old, to):
        for t_user in self.users.values():
            if t_user is not user:
                t_user.SendFrom(user, "NICK", to)
        self.users[to] = user
        del self.users[old]
        if old in self.chanops:
            self.chanops[to] = user
            del self.chanops[old]
        if old in self.voiced:
            self.voiced[to] = user
            del self.voiced[old]

    def _AddMasks(self, list, _list, mask):
        list.append(mask)
        _list.append(Mask(mask))
    def _RemoveMasks(self, list, _list, mask):
        list.remove(mask)
        _list.remove(Mask(mask))

    def AddBan(self, mask): self._AddMasks(self.banlist, self._banlist, mask)
    def RemoveBan(self, mask): self._RemoveMasks(self.banlist, self._banlist, mask)

    def AddBanEx(self, mask): self._AddMasks(self.banexlist, self._banexlist, mask)
    def RemoveBanEx(self, mask): self._RemoveMasks(self.banexlist, self._banexlist, mask)

    def AddInviteEx(self, mask): self._AddMasks(self.invite_ex, self._invite_ex, mask)
    def RemoveInviteEx(self, mask): self._RemoveMasks(self.invite_ex, self._invite_ex, mask)

    def IsBanned(self, user):
        for mask in self._banlist:
            if mask.match(user.prefix):
                for ex_mask in self._banexlist:
                    if ex_mask.match(user.prefix):
                        return False
                return True
        return False

    def CanSpeak(self, user):
        return user.nickname in self.chanops or user.nickname in self.voiced

    def CanJoin(self, user):
        if self.IsBanned(user):
            user.ErrorCode("474", self.name, "Cannot join channel (+b)")
            return False
        elif self.invite_only:
            if user in self.invited:
                return True
            for mask in self._invite_ex:
                if mask.match(user.prefix):
                    return True
            user.ErrorCode("473", self.name, "Cannot join channel (+i)")
            return False
        return True

class IrcProtocol(LineOnlyReceiver):
    def __init__(self, server, address):
        self.server = server

        # User information
        self.nickname = None
        self.username = None
        self.hostname = address.host
        self.prefix = None
        self.registered = False

        # User modes
        self.is_invisible = False # +i
        self.is_away = False
        self.away_message = None

        self.channels = InsensitiveDict()

        # Set when the client sends a QUIT message.
        self.sent_quit = False

    def _UpdatePrefix(self):
        self.prefix = "%s!%s@%s" % (self.nickname, self.username, self.hostname)

    #################### Outgoing messages

    def SendLine(self, line):
        print "[SEND] +++", line
        self.sendLine(line)

    def Send(self, prefix, command, *args):
        args = list(args)
        if args:
            # It would be nice if we could set args[-1] to be trailing parameter
            # unconditionally, but apparently some clients don't like things
            # such as:
            #     :Foo!foo@example.com MODE #channel +v :Bar
            # Requiring instead:
            #     :Foo!foo@example.com MODE #channel +v Bar
            #
            # Also, the last condition is something of a hack to support the
            # empty parameter that TOPIC and ISON will sometimes send.
            if " " in args[-1] or ":" in args[-1] or args[-1] == "":
                args[-1] = ":" + args[-1]
        self.SendLine(":%s %s %s" % (prefix, command, " ".join(args)))

    def SendFrom(self, user, command, *args):
        self.Send(user.prefix, command, *args)

    def FromServer(self, command, *args):
        self.Send(ServerPrefix, command, *args)

    def ErrorCode(self, code, *args):
        if self.nickname:
            self.Send(ServerPrefix, code, self.nickname, *args)
        else:
            self.Send(ServerPrefix, code, *args)

    def AlreadyRegistered(self, command):
        self.ErrorCode("462", "You may not reregister")
    def NotRegistered(self, command):
        self.ErrorCode("451", "You have not registered")

    def NeedMoreParams(self, command):
        self.ErrorCode("461", command, "Not enough parameters")

    def NoSuchChannel(self, name):
        self.ErrorCode("403", name, "No such channel")
    def NotOnChannel(self, name):
        self.ErrorCode("442", name, "You're not on that channel")
    def NotChanop(self, name):
        self.ErrorCode("482", name, "You're not channel operator")
    def NoSuchNickOrChannel(self, nick):
        self.ErrorCode("401", nick, "No such nick/channel")

    def SendAwayNotification(self, user):
        if user.is_away:
            self.ErrorCode("301", user.nickname, user.away_message)

    #################### Incoming messages

    @staticmethod
    def _ParseLine(line):
        if line[0] == ":":
            # Prefix is irrelevant from clients.
            line = line[1:].split(" ", 1)[1]
        if " :" in line:
            line, trailing = line.split(" :", 1)
            trailing = [trailing]
        else:
            trailing = []
        args = line.split() + trailing
        return args[0], args[1:]

    def lineReceived(self, line):
        print "[RECV] ---", line
        o_command, args = IrcProtocol._ParseLine(line)
        command = o_command.upper() # Case insensitivity

        if command == "PASS":
            if self.registered: return self.AlreadyRegistered("PASS")
            if len(args) < 1: return self.NeedMoreParams("PASS")
            return
        elif command == "NICK":
            if len(args) < 1: return self.ErrorCode("431", "No nickname given")
            to = args[0]

            # Check availability.
            if to in self.server.users:
                return self.ErrorCode("433", to, "Nickname is already in use")

            if self.registered:
                old = self.nickname

                self.server.users[to] = self
                del self.server.users[old]

                for channel in self.channels.values():
                    channel.NotifyNickChange(self, old, to)
                self.SendFrom(self, "NICK", to)

                self.nickname = to
                self._UpdatePrefix()
            else:
                self.nickname = to
                if self.username:
                    self.FinishRegistration()
            return
        elif command == "USER":
            if self.registered: return self.AlreadyRegistered("USER")
            if len(args) < 4: return self.NeedMoreParams("USER")
            self.username = args[0]
            # Todo: Pay attention to initial user mode parameter (args[1])
            # This is made more confusing by the fact that args[1] and args[2]
            # have two forms- the old and the new.
            self.realname = args[3]
            if self.nickname:
                # Todo: Check nickname again.
                self.FinishRegistration()
            return

        if not self.registered: return self.NotRegistered(o_command)

        if command == "PING":
            if len(args) < 1: return self.ErrorCode("409", "No origin specified")
            self.FromServer("PONG", ServerPrefix, args[0])
        elif command == "MODE":
            if len(args) < 1: return self.NeedMoreParams("MODE")
            target = args[0]
            if target[0] == "#":
                # This is a fairly complicated command. Unlike user modes, it's
                # not nearly as easy to differentiate between a command to set
                # modes and one to request existing ones (it's even worse
                # because things like "+b" can be both at once).

                # Cache various variables
                channel = self.server.channels.get(target)
                if not channel: return self.NoSuchChannel(name)
                is_on = target in self.channels
                is_op = is_on and self.nickname in channel.chanops

                # See if it's an obvious request (MODE #channel)
                if len(args) < 2:
                    acc = "+"
                    if channel.no_msgs_from_outside: acc += "n"
                    if channel.op_only_topic: acc += "t"
                    if channel.moderated: acc += "m"
                    if channel.invite_only: acc += "i"
                    if channel.secret: acc += "s"
                    return self.ErrorCode("324", target, acc)

                # Modes that can be listed. Example commands:
                #     MODE #channel +b
                #     MODE #channel b
                # You can't request more than one at once, thankfully.
                ListableModes = "beI"

                def ListMode(mode):
                    def _SendList(list, ncode, ecode, enote):
                        for item in list:
                            self.ErrorCode(ncode, target, item)
                        self.ErrorCode(ecode, target, enote)

                    if mode == "b":
                        return _SendList(channel.banlist, "367", "368", "End of channel ban list")
                    elif mode == "e":
                        return _SendList(channel.banexlist, "348", "349", "End of channel exception list")
                    elif mode == "I":
                        return _SendList(channel.invite_ex, "345", "347", "End of channel invite list")

                # See if it's a request for a listable mode.
                if len(args[1]) < 3 and len(args) < 3:
                    # Possibilities:
                    #     MODE #channel b
                    #     MODE #channel +b
                    # Drat this flexibility. Thankfully you can't request more
                    # than one at once.
                    if args[1][-1] in ListableModes:
                        return ListMode(args[1][-1])

                # At this point, it's not a request of any kind. You'd better
                # have ops.
                if not is_op: return self.NotChanop(target)

                m_args = args[2:]
                updates, u_args = {True: "", False: ""}, []
                state = True

                def _HandleParamMode(char, addable, switch, add, remove):
                    if not switch and state:
                        u_args.append(addable)
                        updates[state] += char
                        add()
                    elif switch and not state:
                        u_args.append(addable)
                        updates[state] += char
                        remove()

                def _OpVoice(char, dict):
                    if not m_args: return
                    target = m_args.pop(0)

                    user = self.server.users.get(target)
                    if user:
                        def _Add(): dict[target] = user
                        def _Remove(): del dict[target]
                        _HandleParamMode(char, target, target in dict, _Add, _Remove)
                    else:
                        self.NoSuchNickOrChannel(target)

                def _ListMod(char, adder, remover):
                    if not m_args: return
                    mask = m_args.pop(0)

                    _HandleParamMode(char, mask, mask in channel.banlist,
                                     lambda: adder(mask), lambda: remover(mask))

                for char in args[1]:
                    if char == "+": state = True
                    elif char == "-": state = False
                    elif char == "t":
                        channel.op_only_topic = state
                        updates[state] += "t"
                    elif char == "n":
                        channel.no_msgs_from_outside = state
                        updates[state] += "n"
                    elif char == "m":
                        channel.moderated = state
                        updates[state] += "m"
                    elif char == "i":
                        channel.invite_only = state
                        updates[state] += "i"
                    elif char == "s":
                        channel.secret = state
                        updates[state] += "s"
                    elif char == "o": _OpVoice("o", channel.chanops)
                    elif char == "v": _OpVoice("v", channel.voiced)
                    elif char == "b": _ListMod("b", channel.AddBan, channel.RemoveBan)
                    elif char == "e": _ListMod("e", channel.AddBanEx,
                                               channel.RemoveBanEx)
                    elif char == "I": _ListMod("I", channel.AddInviteEx,
                                               channel.RemoveInviteEx)
                    else: self.ErrorCode("472", char, "is unknown mode char to me")

                acc = ""
                if updates[True]:
                    acc += "+" + updates[True]
                if updates[False]:
                    acc += "-" + updates[False]
                if acc:
                    for t_user in channel.users.values():
                        t_user.SendFrom(self, "MODE", target, acc, *u_args)
            else:
                if target.lower() != self.nickname.lower():
                    return self.ErrorCode("502", "Cannot change mode for other users")

                if len(args) > 1:
                    # Fixme: This doesn't properly handle things like "+i-i".
                    # Possible solution: For each char in updates[True], remove
                    # both if it is also in updates[False].

                    updates = {True: "", False: ""}
                    state, invalid = True, False

                    for char in args[1]:
                        if char == "+": state = True
                        elif char == "-": state = False
                        elif char == "i":
                            if self.is_invisible != state:
                                self.is_invisible = state
                                updates[state] += "i"
                        else: invalid = True

                    acc = ""
                    if updates[True]: acc += "+" + updates[True]
                    if updates[False]: acc += "-" + updates[False]

                    if invalid: self.ErrorCode("501", "Unknown MODE flag")
                    if acc: self.SendFrom(self, "MODE", self.nickname, acc)
                else:
                    # Request
                    acc = "+"
                    if self.is_invisible: acc += "i"
                    self.ErrorCode("221", acc)
        elif command == "QUIT":
            if not len(args) or not args[0]:
                message = "Client quit"
            else:
                message = "Quit: \"%s\"" % args[0]
            self.FromServer("ERROR", "Closing link: %s" % message)
            self.sent_quit = True
            self.transport.loseConnection()
            self.FinishQuit(message)
        elif command == "JOIN":
            if len(args) < 1: return self.NeedMoreParams("JOIN")
            name = args[0]

            if name in self.channels: return

            if name in self.server.channels:
                channel = self.server.channels[name]

                if not channel.CanJoin(self):
                    return

                channel.JoinUser(self)
            else:
                channel = Channel(self.server, name)
                channel.JoinInitialUser(self)
                self.server.channels[name] = channel

            self.channels[name] = channel
        elif command == "NAMES":
            # Can't NAMES the whole server.
            if len(args) < 1: return
            name = args[0]
            channel = self.server.channels.get(name)
            if channel:
                channel.SendNames(self)
        elif command == "PART":
            if len(args) < 1: return self.NeedMoreParams("PART")
            name = args[0]

            if name not in self.server.channels: return self.NoSuchChannel(name)
            if name not in self.channels: return self.NotOnChannel(name)

            if len(args) > 1: message = '"' + args[1] + '"'
            else: message = None

            self.channels[name].NotifyPart(self, message)
            del self.channels[name]
        elif command == "TOPIC":
            if len(args) < 1: return self.NeedMoreParams("TOPIC")
            name = args[0]

            channel = self.server.channels.get(name)
            if not channel: return self.NoSuchChannel(name)

            if len(args) > 1:
                if name not in self.channels: return self.NotOnChannel(name)

                to = args[1]

                if channel.op_only_topic and self.nickname not in channel.chanops:
                    return NotChanop(name)

                channel.SetTopic(self, to)
            else:
                channel.SendTopic(self)
        elif command == "KICK":
            if len(args) < 2: return self.NeedMoreParams("KICK")
            name = args[0]
            if name not in self.server.channels: return self.NoSuchChannel(name)
            channel = self.channels.get(name)
            if not channel: return self.NotOnChannel(name)
            if self.nickname not in channel.chanops: return self.NotChanop(name)

            nickname = args[1]
            comment = args[2] if (len(args) > 2 and args[2]) else self.nickname

            user = self.server.users.get(nickname)
            if not user: return self.NoSuchNickOrChannel(nickname)

            if name not in user.channels:
                return self.ErrorCode("441", nickname, name, "They aren't on that channel")

            channel.NotifyKick(self, user, comment)
            del user.channels[name]
        elif command == "INVITE":
            # Fixme: Can you *really* invite somebody to a channel without
            # chanops? It works with Unreal, but I didn't think Freenode let
            # you!
            if len(args) < 2: return self.NeedMoreParams("INVITE")
            nickname, target = args[0], args[1]

            user = self.server.users.get(nickname)
            if not user: return self.NoSuchNickOrChannel(nickname)

            channel = self.channels.get(target)
            if not channel: return self.NoSuchChannel(target)

            if target not in self.channels: return self.NotOnChannel(target)

            if nickname in channel.users:
                return self.ErrorCode("443", nickname, target, "is already on channel")

            if channel.invite_only and self.nickname not in channel.chanops:
                return self.NotChanop(target)

            user.SendFrom(self, "INVITE", self.nickname, target)

            # Weird. The RFC says that the order is <channel> <nick>, but
            # everybody else seems to do it this way!
            self.ErrorCode("341", nickname, target)

            if user not in channel.invited: channel.invited.append(user)

            self.SendAwayNotification(user)
        elif command == "PRIVMSG" or command == "NOTICE":
            # These commands have precisely the same operation, so we don't
            # bother with a separate command.
            if not len(args): return self.ErrorCode("411", "No recipient given (%s)" % o_command)
            if len(args) < 2: return self.ErrorCode("412", "No text to send")
            target, message = args[0], args[1]

            if target[0] == "#":
                channel = self.server.channels.get(target)
                if not channel:
                    return self.NoSuchNickOrChannel(target)

                if target not in self.channels and channel.no_msgs_from_outside: # +n
                    return self.ErrorCode("404", target, "No external channel messages")
                elif channel.moderated and not channel.CanSpeak(self): # +m
                    return self.ErrorCode("404", target, "You need voice")
                elif channel.IsBanned(self) and not channel.CanSpeak(self): # +b
                    return self.ErrorCode("404", target, "You are banned")

                channel.SendMessage(self, message)
            else:
                user = self.server.users.get(target)
                if not user:
                    return self.NoSuchNickOrChannel(target)
                user.SendFrom(self, command, target, message)

                self.SendAwayNotification(user)
        elif command == "AWAY":
            if not len(args):
                if self.is_away:
                    self.ErrorCode("305", "You are no longer marked as being away")
                else:
                    self.ErrorCode("306", "You have been marked as being away")
                self.is_away = not self.is_away
            else:
                self.is_away = True
                self.away_message = args[0]
                self.ErrorCode("306", "You have been marked as being away")
        elif command == "WHO":
            # Can't /WHO the entire server.
            if not len(args): return
            target = args[0]

            if target[0] == "#":
                channel = self.server.channels.get(target)
                # Do not send secret channels unless you are on them.
                if channel and (not channel.secret or self.nickname in channel.users):
                    for nickname, user in channel.users.items():
                        # Do not send invisible users, unless you're on the channel.
                        if user.is_invisible and not self.nickname in channel.users:
                            continue

                        if user.is_away: mode = "G"
                        else: mode = "H"

                        if nickname in channel.chanops: mode += "@"
                        elif nickname in channel.voiced: mode += "+"

                        self.ErrorCode("352", target, user.username,
                                       user.hostname, ServerPrefix, user.nickname,
                                       mode, "0 " + user.realname)
                self.ErrorCode("315", target, "End of /WHO list")
            else:
                # How do you run /WHO on a single user?
                pass
        elif command == "WHOIS":
            if len(args) < 1: return self.ErrorCode("431", "No nickname given")

            user = self.server.users.get(args[0])
            if not user:
                return self.NoSuchNickOrChannel(args[0])

            self.ErrorCode("311", user.nickname, user.username, user.hostname,
                           "*", user.realname)

            self.ErrorCode("312", user.nickname, ServerPrefix, ServerInfo)

            acc = ""
            for name, channel in user.channels.items():
                if user.is_invisible and name not in self.channels: # +i user mode
                    continue
                if channel.secret and self.nickname not in channel.users: # +s channel mode
                    continue
                if user.nickname in channel.chanops: acc += "@"
                elif user.nickname in channel.voiced: acc += "+"
                acc += name + " "
            if acc:
                self.ErrorCode("319", user.nickname, acc)

            self.SendAwayNotification(user)

            self.ErrorCode("318", user.nickname, "End of /WHOIS list")
        elif command == "ISON":
            if len(args) < 1: return self.NeedMoreParams("ISON")
            acc = ""
            for nickname in args:
                if nickname in self.server.users:
                    acc += nickname + " "
            self.ErrorCode("303", acc)
        elif command == "LUSERS":
            self.SendLusers()
        elif command == "MOTD":
            self.SendMotd()
        elif command == "VERSION":
            self.ErrorCode("351", "%s. %s :" % (PyIrcdVersion, ServerPrefix))
            self.ISupport()
        elif command == "TIME":
            self.ErrorCode("391", ServerPrefix, time.strftime("%c"))
        elif command == "INFO":
            # This is completely free-form, except for the end marker.
            def Info(info): self.ErrorCode("371", info)

            Info(PyIrcdVersion)
            # Anything else we want to share?
            # - Authors and contributors
            # - Website
            # - Date of installation (compilation date doesn't really apply)
            # - Start time
            # - VCS revision number

            self.ErrorCode("374", "End of /INFO list")
        else:
            return self.ErrorCode("421", o_command, "Unknown command")

    def connectionLost(self, reason):
        if not self.sent_quit:
            # And consequently QUIT messages haven't been sent around...
            self.FinishQuit("Remote closed the connection")

    def FinishRegistration(self):
        self.registered = True
        self._UpdatePrefix()
        self.server.users[self.nickname] = self
        self.ErrorCode("001", "Welcome to the Internet Relay Network %s" % self.prefix)
        self.ErrorCode("002", "Your host is %s, running version %s" % (ServerPrefix, PyIrcdVersion))
        self.ErrorCode("003", "This server was created %s" % CreationTimestamp)
        self.Send(ServerPrefix, "004", self.nickname, ServerPrefix, PyIrcdVersion, AvailUserModes, AvailChannelModes)
        self.ISupport()
        self.SendLusers()
        # Todo: Send ADMINS
        self.SendMotd()

    def ISupport(self):
        # Todo
        pass

    def SendLusers(self):
        # This isn't quite the way it's supposed to be; it leaves out
        # server/service information. But there is none...

        users, channels = len(self.server.users), len(self.server.channels)

        self.ErrorCode("251", "There are %i users" % users)
        if channels: self.ErrorCode("254", str(channels), "channels formed")
        self.ErrorCode("255", "I have %s clients" % users)

    def SendMotd(self):
        # Todo
        pass

    def FinishQuit(self, message):
        if self.registered:
            del self.server.users[self.nickname]
        for channel in self.channels.values():
            channel.NotifyQuit(self, message)

class IrcFactory(Factory):
    def __init__(self, server):
        self.server = server

    def buildProtocol(self, address):
        return IrcProtocol(self.server, address)

class IrcServer:
    def __init__(self):
        self.users = InsensitiveDict()
        self.channels = InsensitiveDict()

def Main():
    server = IrcServer()
    factory = IrcFactory(server)
    reactor.listenTCP(8002, factory)
    reactor.run()

if __name__ == "__main__":
    Main()
