
                                     PyIRCd

1) Requirements

    - Python 2.5 or later
    - Twisted

2) License

    GPL v3. See doc/GPLv3.txt.

3) Installation

    To install from the source tarball, run:

        sudo ./setup.py install

    To install to a different directory than the default:

        ./setup.py install --prefix=/installation/point

4) Configuration

    Create a directory to store its configuration files, MOTD, logs, etc. Into
    this directory copy the example configuration file and edit, or write one
    from scratch.

    Configuration files are standard YAML syntax.

    Note: Because passwords are stored in plaintext, remember to set the
    permissions on your configuration files as tightly as possible:

        chmod 600 config.yaml
        chmod 600 manhole.auth    # If you use this service, see below.

5) Starting the server

    Change directory to the location of the configuration file, and run:

        twistd pyircd

    If your configuration file is not named "config.yaml", or not in the
    current directory, use the -f option:

        twistd pyircd -f myconfig.yaml
        twistd pyircd --file myconfig.yaml

    The server will change directory to the location of the configuration file,
    so all paths in the file are relative to its location.

    Using the epoll reactor is recommended if you have Linux 2.6 or later (note
    that the -r option must come before "pyircd"):

        twistd -r epoll pyircd

    You may wish to look into alternative reactors if your platform supports
    them. Type this to find out which are available:

        twistd --help-reactors

6) Stopping the server

        kill `cat twistd.pid`

6) Crashes

    If you ever see somebody quit with a message like this:

        <-- Foo has quit (Server crash)

    Or, for some reason, you find a file named "traceback.log" in the top level
    directory, it means somebody has managed to crash the server (to a point).
    Kindly submit a bug, and include the contents of that file.

7) Remote debugging

            Warning: Manhole access is experimental and may not be
            fully secure. Please continue with caution.

    Twisted has a remote access tool named Manhole that PyIRCd can enable upon
    request in the configuration file. If this is enabled, you must create a
    file named "manhole.auth" in the configuration directory with passwd(5)
    syntax. For example:

        foouser:barpassw0rd:::::

    The default port is 2022, however this can be changed in the configuration
    file. After starting PyIRCd, you can SSH "into the server" to get a Python
    prompt:

        ssh localhost -p 2022

    Type the password you set in manhole.auth.

    At this prompt, four variables are made available:

        service: The PyIRCdService object. This is the entry point into the
                 rest of the server.
        server: The Server object, which contains most objects in PyIRCd.
        channels: A case-insensitive dictionary mapping all existing channel
                  names to their Channel object.
        users: A case-insensitive dictionary mapping all connected nicknames to
               their Client object.

    Now that you're in the server, you can do anything you like. For example,
    to deop somebody on channel #foo:

        # Grab a reference to the channel object
        foo = channels["#foo"]
        # Remove bar from the list of chanops
        del foo.chanops["bar"]
        # Notify users of the mode change.
        foo.SendToAll(server, "MODE #foo -o bar")
