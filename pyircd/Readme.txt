An old IRCd I wrote once upon a time for the fun of it. Though a quick Google
search shows there are now quite a few similar daemons out there, at the time
this was written (2009), I couldn't find any other Python IRCd's at all. Though
it would be highly presumptious of me to claim to have been the original, I
suspect it may have been one of the first few to reach Google.

The code is straightforward for the most part, if not somewhat inflexible- this
is far from a modern, modular ircd (though it's hard to do better in that
department than daemons like InspIRCd have).

It implements a dialect of IRC somewhere between the RFC's and Hyperion, making
it appear somewhat archaic next to modern daemons (such as ircd-seven or
InspIRCd, the ones I deal with most often today).

Most notable among the things it does *not* implement is a server protocol- it
stands strictly on its own. If I were to revisit this project I would probably
take modularity and server-server support into consideration from the beginning,
as both would have far-reaching impacts on the codebase and would be quite
difficult to add from its current state- amounting to a rewrite anyway.

The original Readme I wrote for the project is in Readme.orig.txt.
