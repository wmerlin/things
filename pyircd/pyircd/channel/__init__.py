# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from pyircd import mode, util
from pyircd.channel import modes

class ChannelModes(mode.Modes):
    def OnUnknown(self, user, char):
        user.ErrorCode("472", char, "is unknown mode char to me")

class Channel(object):
    """Represents a channel."""

    def __init__(self, server, name):
        self.server = server
        self.name = name
        self.topic = None

        # Modes
        self.no_outside_msgs = mode.Flag("n")
        self.op_only_topic = mode.Flag("t")
        self.secret = mode.Flag("s")
        self.moderated = mode.Flag("m")
        self.invite_only = mode.Flag("i")

        self.limit = modes.UserLimit(self)
        self.has_limit = False
        self.user_limit = None
        self.key = modes.ChannelKey(self)
        self.has_key = False
        self.channel_key = None

        self.non_op_invite = mode.Flag("g")
        self.no_ctcp = mode.Flag("C")
        self.no_notices = mode.Flag("T")

        self.invite_ex = modes.MaskList("I")
        self.bans = modes.MaskList("b")
        self.ban_ex = modes.MaskList("e")

        self.users = util.IDict()
        self.chanops = util.IDict()
        self.voiced = util.IDict()

        self.modes = ChannelModes(n=self.no_outside_msgs, t=self.op_only_topic,
                                  s=self.secret, m=self.moderated,
                                  l=self.limit, k=self.key,
                                  C=self.no_ctcp, T=self.no_notices,
                                  g=self.non_op_invite,
                                  i=self.invite_only, I=self.invite_ex,
                                  b=self.bans, e=self.ban_ex,
                                  o=modes.UserList(self, self.chanops, "o"),
                                  v=modes.UserList(self, self.voiced, "v"))

        self.invited = []

    def SendToAll(self, user, command, *args):
        """Sends a message to all inhabitants of the channel."""
        for t_user in self.users.values():
            t_user.SendFrom(user, command, *args)

    def SendToOthers(self, user, command, *args):
        """Sends a message to all *other* inhabitants of a channel."""
        for t_user in self.users.values():
            if t_user is not user:
                t_user.SendFrom(user, command, *args)

    def SendMessage(self, by, command, message):
        """Sends a PRIVMSG."""
        self.SendToOthers(by, command, self.name, message)

    def NotifyNickChange(self, user, old, nickname):
        """Notifies clients of a nickname change and updates internal state."""
        self.SendToOthers(user, "NICK", nickname)

        self.users[nickname] = user
        del self.users[old]
        if old in self.chanops:
            self.chanops[nickname] = user
            del self.chanops[old]
        if old in self.voiced:
            self.voiced[nickname] = user
            del self.voiced[old]

    def _HasVoice(self, user):
        """Internal function to determine whether a user has either voice or
        chanops."""
        nickname = user.nickname
        return nickname in self.voiced or nickname in self.chanops

    def _IsBanned(self, user):
        """Check whether a user is banned. Takes ban exceptions into account."""
        if self.bans.Matches(user.prefix):
            if self.ban_ex.Matches(user.prefix):
                return False
            return True
        return False

    def CanJoin(self, user, key):
        """Determine whether a user can join this channel.

        Perhaps in bad design, this has the side effect of sending the proper
        error code to the user."""
        if user in self.invited:
            return True
        if self.has_limit and len(self.users) >= self.user_limit:
            user.ErrorCode("471", self.name, "Cannot join channel (+l)")
            return False
        if self.has_key and key != self.channel_key:
            user.ErrorCode("475", self.name, "Cannot join channel (+k)")
            return False
        if self.invite_only:
            if self.invite_ex.Matches(user.prefix):
                return True
            user.ErrorCode("473", self.name, "Cannot join channel (+i)")
            return False
        if self._IsBanned(user):
            user.ErrorCode("474", self.name, "Cannot join channel (+b)")
            return False
        return True

    def CanSpeak(self, user):
        """Determine whether a user can send a message to this channel.

        Has the side effect of sending error codes if this is not allowed."""
        nickname = user.nickname
        if self.no_outside_msgs and nickname not in self.users:
            user.ErrorCode("404", self.name, "No external channel messages")
            return False
        if self.moderated and not self._HasVoice(user):
            user.ErrorCode("404", self.name, "You need voice")
            return False
        if self._IsBanned(user) and not self._HasVoice(user):
            user.ErrorCode("404", self.name, "You are banned")
            return False
        return True

    def CanSend(self, user, message):
        """Determine whether a particular message can be sent to this channel."""
        if not self.CanSpeak(user):
            return False

        if self.no_ctcp and message[0] == "\x01" and user.nickname not in self.chanops:
            type = message[1:].split(" ", 1)[0]
            if type == "ACTION":
                return True

            user.ErrorCode("404", self.name, "CTCPs are not permitted in this channel")
            return False
        return True

    def CanInvite(self, user):
        """Determine whether a user may send an invite."""
        if user.nickname not in self.chanops:
            if self.invite_only:
                user.NotChanop(self.name)
                return False

            if self.non_op_invite:
                return True

            user.NotChanop(self.name)
            return False
        return True

    def JoinUser(self, user):
        """Makes a user join the channel. Sends JOIN messages to all users
        (including this one)."""
        self.users[user.nickname] = user
        self.SendToAll(user, "JOIN", self.name)
        if self.topic:
            self.SendTopic(user)
        self.SendNames(user)
        if user in self.invited:
            self.invited.remove(user)

    def JoinInitialUser(self, user):
        """Joins the first user.

        Mostly equivalent to JoinUser(), but gives the user chanops and sets
        initial channel modes."""
        self.users[user.nickname] = user
        self.chanops[user.nickname] = user
        user.FromSelf("JOIN", self.name)
        self.SendNames(user)

        modes, args = self.modes.Process(self.server,
                                         self.server.def_channel_modes,
                                         self.server.def_channel_args)
        if modes:
            self.SendToAll(self.server, "MODE", self.name, modes, *args)

    def SendTopic(self, user):
        """Sends the current topic to a user."""
        if self.topic is None:
            # This code appears to be used only until the topic is set for the
            # first time.
            user.ErrorCode("331", self.name, "No topic is set.")
        else:
            user.ErrorCode("332", self.name, self.topic)

    def SendNames(self, user):
        """Sends a NAMES list to a user."""
        is_on = user.nickname in self.users

        # Do not display users in secret channels unless they are on it.
        if not is_on and self.secret:
            return user.ErrorCode("366", self.name, "End of /NAMES list")

        acc = ""
        for nickname, t_user in self.users.items():
            # Do not list invisible users.
            if not is_on and t_user.is_invisible:
                continue

            if nickname in self.chanops:
                acc += "@"
            elif nickname in self.voiced:
                acc += "+"
            acc += nickname + " "
        if acc:
            user.ErrorCode("353", "=", self.name, acc)
        user.ErrorCode("366", self.name, "End of /NAMES list")

    def _RemoveUser(self, user):
        """Internal user to remove a user from the channel."""
        nickname = user.nickname
        del self.users[nickname]

        if nickname in self.chanops:
            del self.chanops[nickname]
        if nickname in self.voiced:
            del self.voiced[nickname]
        if user in self.invited:
            self.invited.remove(user)

    def _DeleteIfEmpty(self):
        """Internal function to delete a channel if it is empty."""
        if not self.users:
            del self.server.channels[self.name]

    def QuitUser(self, user, message):
        """Notifies other clients that another user has quit IRC."""
        self._RemoveUser(user)
        self.SendToAll(user, "QUIT", message)
        self._DeleteIfEmpty()

    def PartUser(self, user, message):
        """Notifies other clients that a user has left the channel."""
        if message:
            self.SendToAll(user, "PART", self.name, message)
        else:
            self.SendToAll(user, "PART", self.name)
        self._RemoveUser(user)
        self._DeleteIfEmpty()

    def KickUser(self, by, user, message):
        """Kicks a user off a channel."""
        self.SendToAll(by, "KICK", self.name, user.nickname, message)
        self._RemoveUser(user)
        self._DeleteIfEmpty()

    def SetTopic(self, user, to):
        """Sets the topic."""
        self.topic = to
        self.SendToAll(user, "TOPIC", self.name, to)
