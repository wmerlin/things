# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from pyircd import mode, util

class MaskList(mode.ParamMode):
    """A generic mode for lists of user masks.

    Its only public method is Matches(), which determines whether any mask in
    its list matches a certain prefix."""

    def __init__(self, char):
        mode.ParamMode.__init__(self, char)
        self.masks = util.IDict()

    def Add(self, user, data, mask):
        if mask in data:
            if not data[mask][0]:
                del self.masks[mask]
                del data[mask]
        else:
            imask = util.IMask(mask)
            self.masks[mask] = imask
            data[mask] = (True, imask)

    def Remove(self, user, data, mask):
        if mask in data:
            if data[mask][0]:
                del self.masks[mask]
                del data[mask]
        else:
            del self.masks[mask]
            data[mask] = (False, util.IMask(mask))

    def Default(self):
        # mask -> (added?, IMask)
        return util.IDict()

    def Interpret(self, data):
        added, removed, args = "", "", []
        for mask, val in data.items():
            if val[0]:
                added += self.char
            else:
                removed += self.char
            args.append(mask)
        return added, removed, args

    def Matches(self, prefix):
        for mask in self.masks.values():
            if mask.match(prefix):
                return True
        return False

class UserList(mode.ParamMode):
    """A list of users."""

    def __init__(self, channel, users, char):
        mode.ParamMode.__init__(self, char)
        self.server = channel.server
        self.channel = channel
        self.users = users

    def Add(self, t_user, data, nickname):
        if nickname in data:
            if not data[nickname]:
                del self.users[nickname]
                del data[nickname]
        else:
            user = self.server.users.get(nickname)
            if not user:
                return t_user.NoSuchNickOrChannel(nickname)
            if nickname not in self.channel.users:
                return t_user.ErrorCode("441", nickname, self.channel.name,
                                        "They aren't on that channel")

            self.users[nickname] = user
            data[nickname] = True

    def Remove(self, t_user, data, nickname):
        if nickname in data:
            if data[nickname]:
                del self.users[nickname]
                del data[nickname]
        else:
            if nickname not in self.server.users:
                return t_user.NoSuchNickOrChannel(nickname)
            if nickname not in self.channel.users:
                return t_user.ErrorCode("441", nickname, self.channel.name,
                                        "They aren't on that channel")

            try:
                del self.users[nickname]
                data[nickname] = False
            except KeyError:
                # Happens sometimes when an oper uses their override to deop
                # somebody who's not chanop.
                pass

    def Default(self):
        # IDict: nickname -> added?
        return util.IDict()

    def Interpret(self, data):
        added, removed, args = "", "", []
        for nickname, val in data.items():
            if val:
                added += self.char
            else:
                removed += self.char
            args.append(nickname)
        return added, removed, args

class SetParamMode(mode.Mode):
    """A mode that takes a parameter when being set, but not when unset."""
    def Process(self, user, state, data, args):
        if state:
            if not args:
                return
            arg = args.pop(0)

            self.Set(data, arg)
        else:
            self.Unset(data)

    def Default(self):
        # added? value
        return [None, None]

class UserLimit(SetParamMode):
    def __init__(self, channel):
        SetParamMode.__init__(self, "l")
        self.channel = channel

    def Set(self, data, arg):
        try:
            limit = int(arg)
        except ValueError:
            data[0] = False
            self.channel.has_limit = False
            return

        self.channel.has_limit = True
        self.channel.user_limit = limit
        data[0] = True
        data[1] = limit

    def Unset(self, data):
        data[0] = False
        self.channel.has_limit = False

    def State(self, user):
        if self.channel.has_limit:
            if user.nickname in self.channel.users:
                return "l", [str(self.channel.user_limit)]
            return "l", []
        return "", []

    def Interpret(self, data):
        if data[0] is None:
            return "", "", []
        elif data[0] is True:
            return "l", "", [str(data[1])]
        else:
            return "", "l", []

class ChannelKey(SetParamMode):
    def __init__(self, channel):
        SetParamMode.__init__(self, "k")
        self.channel = channel

    def Set(self, data, arg):
        self.channel.has_key = True
        self.channel.channel_key = arg
        data[0] = True
        data[1] = arg

    def Unset(self, data):
        data[0] = False
        self.channel.has_key = False

    def State(self, user):
        if self.channel.has_key:
            if user.nickname in self.channel.users:
                return "k", [self.channel.channel_key]
            return "k", []
        return "", []

    def Interpret(self, data):
        if data[0] is None:
            return "", "", []
        elif data[0] is True:
            return "k", "", [data[1]]
        else:
            return "", "k", []
