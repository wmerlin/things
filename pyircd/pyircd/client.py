# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from twisted.internet import reactor, threads
from pyircd.protocol import Protocol
import pyircd.commands
from pyircd import log, mode
import pyircd.util
import socket
import traceback

class UserModes(mode.Modes):
    def __init__(self, user, **modes):
        self.user = user
        mode.Modes.__init__(self, **modes)

    def Process(self, user, modes, args):
        self.invalid = False
        return mode.Modes.Process(self, user, modes, args)

    def OnUnknown(self, user, char):
        if not self.invalid:
            user.ErrorCode("501", "Unknown MODE flag")
            self.invalid = True

class OperFlag(mode.Flag):
    """Operator flag.

    Just a normal flag, except you can't set it."""

    def __init__(self):
        mode.Flag.__init__(self, "o")

    def Set(self, user, updates):
        pass

class Client(Protocol):
    """Represents a client."""

    def __init__(self, server, address):
        Protocol.__init__(self, server)
        self.dc_state = False
        self.ip_address = address.host

    def SetHostname(self, info):
        self.hostname = info[0]
        self.MaybeRegister()

    def connectionMade(self):
        # Check IP against Z-Lines.
        for pattern, reason in self.server.zlines:
            if pattern.match(self.ip_address):
                self.dc_state = True
                log.Log(log.BanKill, "Z-Lined connection from %s: %s" %
                        (self.ip_address, reason))
                self.SendError("Z-Lined: %s" % reason)
                self.DropConnection()

        # Look up hostname.
        deferred = threads.deferToThread(socket.getnameinfo, (self.ip_address, 0), 0)
        deferred.addCallback(self.SetHostname)

        # Small hack to make error codes work "correctly" before registration.
        self.nickname = ""
        self.username = None
        self.realname = None
        self.hostname = None
        self.prefix = None
        self.registered = False

        # Modes
        self.is_invisible = mode.Flag("i")
        self.is_oper = OperFlag()
        self.hears_wallops = mode.Flag("w")
        self.modes = UserModes(self, i=self.is_invisible, o=self.is_oper,
                                     w=self.hears_wallops)

        self.is_away = False
        self.away_message = None

        self.channels = pyircd.util.IDict()

        # Set to True if the QUIT messages have been sent to channels. Used to
        # determine whether to notify other clients when the connection is
        # suddenly lost.
        self.sent_quit = False

        self.timeout_timer = reactor.callLater(self.server.ping_interval, self.Timeout)

    ########################################################################
    # Various commonly used error codes

    def NoNicknameGiven(self):
        self.ErrorCode("431", "No nickname given")
    def NeedMoreParams(self, command):
        self.ErrorCode("461", command, "Not enough parameters")

    def NoSuchNickOrChannel(self, name):
        self.ErrorCode("401", name, "No such nick/channel")
    def NoSuchChannel(self, name):
        self.ErrorCode("403", name, "No such channel")
    def NotOnChannel(self, name):
        self.ErrorCode("442", name, "You're not on that channel")
    def NotChanop(self, name):
        self.ErrorCode("482", name, "You're not channel operator")

    def SendAwayNote(self, user):
        if user.is_away:
            self.ErrorCode("301", user.nickname, user.away_message)

    def NotOperator(self):
        self.ErrorCode("481", "Permission denied- You're not an IRC operator")

    ########################################################################
    # Timeout logic

    def SendPing(self):
        """This is called when the timer is triggered for the first time, to
        send a PING message."""
        self.FromServer("PING", self.server.prefix)
        self.timeout_timer = reactor.callLater(self.server.ping_interval, self.Timeout)

    def Timeout(self):
        """This is called when the timer is triggered the second time. The
        client has not responded for a long enough duration, so we close the
        connection."""
        self.dc_state = True
        self.LogQuit()
        self.SendError("Ping timeout")
        self.NotifyQuit("Ping timeout")
        self.Deregister()
        self.StopTimer()
        self.DropConnection()

    ########################################################################
    # Command processing

    def ProcessCommand(self, _command, args):
        self.timeout_timer.cancel()
        self.timeout_timer = reactor.callLater(self.server.ping_interval, self.SendPing)

        command = _command.upper()

        if command not in ["PASS", "NICK", "USER"] and not self.registered:
            return self.ErrorCode("451", _command, "You have not registered")

        callback = pyircd.commands.Table.get(command)

        if not callback:
            return self.ErrorCode("421", _command, "Unknown command")

        if command not in self.server.command_stats:
            self.server.command_stats[command] = 0
        self.server.command_stats[command] += 1

        try:
            callback(self, args)
        except:
            log.Log(log.Crash, "Server crash (see traceback.log)")
            t_log = open("traceback.log", "a")
            print >>t_log, "-" * 80
            print >>t_log, "Remote address:", self.ip_address
            print >>t_log, "User:", self.prefix
            print >>t_log, "Command:", _command
            print >>t_log, "Arguments:", args
            # What other useful information can be added here?

            print >>t_log
            traceback.print_exc(file=t_log)
            print >>t_log

            # Disconnect user. Note that if server state has been corrupted, it
            # becomes quite likely for a nested exception to go off soon, even
            # at this moment as we send quit messages. In this case it's not
            # our concern to catch it, as it is not the root cause of the
            # problem.

            message = "Server crash"
            self.dc_state = True
            self.LogQuit()
            self.SendError(message)
            self.NotifyQuit(message)
            self.Deregister()
            self.StopTimer()
            self.DropConnection()

            # Todo: Perhaps send notifications to operators?

    ########################################################################
    # Registration sequence

    def _UpdatePrefix(self):
        """Set the prefix.

        Called whenever it changes (for any reason), to keep the cached prefix
        updated."""
        self.prefix = "%s!%s@%s" % (self.nickname, self.username, self.hostname)

    def CheckBan(self):
        # Check hostname against K-Lines and n-lines.
        banned, reason = False, None

        mask = self.username + "@" + self.hostname
        for pattern, p_reason in self.server.klines:
            if pattern.match(mask):
                banned, type, reason = True, "K-Lined", p_reason
                log.Log(log.BanKill, "K-Lined connection from %s (%s): %s" %
                        (self.ip_address, self.prefix, p_reason))
                break

        for pattern, p_reason in self.server.nlines:
            if pattern.match(self.realname):
                banned, type, reason = True, "N-Lined", p_reason
                log.Log(log.BanKill, "N-Lined connection from %s (%s) \"%s\": %s" %
                        (self.ip_address, self.prefix, self.realname, p_reason))
                break

        if banned:
            self.dc_state = True
            self.ErrorCode("465", "%s: %s" % (type, reason))
            self.StopTimer()
            self.DropConnection()
            return False

        return banned

    def MaybeRegister(self):
        if self.username is not None and self.hostname is not None:
            if self.CheckBan():
                return

            if not self.nickname:
                return

            # Todo: Check nickname again. Consider this scenario:
            #     Client A sends NICK, "foo".
            #     Client B sends NICK, "foo".
            #     Client B sends USER, and registers.
            #     Client A sends USER.
            # What happens? Unreal's solution, for example, is to drop A as
            # soon as B sends NICK.
            self.FinishRegistration()

    def FinishRegistration(self):
        """Called to complete connection registration. It inserts the user into
        the global user database, sends the MOTD, and other related things."""
        self.registered = True
        self._UpdatePrefix()
        self.server.users[self.nickname] = self

        log.Log(log.Connection, "Connection from %s (%s)" % (self.ip_address, self.prefix))

        self.ErrorCode("001", "Welcome to the Internet Relay Network %s" % self.prefix)
        self.ErrorCode("002", "Your host is %s, running version %s" %
                       (self.server.prefix, self.server.PyIRCdVersion))
        self.ErrorCode("003", "This server was created %s" % self.server.creation_timestamp)
        self.ErrorCode("004", self.server.prefix, self.server.PyIRCdVersion,
                       self.server.AvailUserModes, self.server.AvailChannelModes)

        self.SendISupport()
        self.SendLusers()
        self.SendMotd()

        modes, args = self.modes.Process(self.server, self.server.def_user_modes, [])
        if modes:
            self.FromServer("MODE", self.nickname, modes)

    def SendISupport(self):
        # Todo.
        pass

    def SendLusers(self):
        # This isn't quite the way it's supposed to be; it leaves out
        # server/service information. But there is none...

        users, channels = len(self.server.users), len(self.server.channels)

        self.ErrorCode("251", "There are %i users" % users)
        if len(self.server.operators):
            self.ErrorCode("252", str(len(self.server.operators)), "operator(s) online")
        if channels:
            self.ErrorCode("254", str(channels), "channels formed")
        self.ErrorCode("255", "I have %s clients" % users)

    def SendMotd(self):
        if self.server.has_motd:
            self.ErrorCode("375", "- %s Messsage of the day -" % self.server.prefix)
            for line in self.server.motd:
                self.ErrorCode("372", "- " + line)
            self.ErrorCode("376", "End of MOTD command")
        else:
            self.ErrorCode("422", "MOTD File is missing")

    ########################################################################
    # Disconnection sequence.
    # 1) Log
    # 2) Send error
    # 3) Notify quit
    # 4) Deregister
    # 5) Stop timer
    # 6) Drop connection

    def LogQuit(self):
        log.Log(log.Disconnection, "Disconnect: %s" %
                (self.prefix if self.prefix else self.ip_address))

    def SendError(self, message):
        """Sends the ERROR command."""
        self.FromServer("ERROR", "Closing link: " + message)

    def NotifyQuit(self, message):
        """Notifies interested channels that this user has quit."""
        if not self.sent_quit:
            for channel in self.channels.values():
                channel.QuitUser(self, message)
            self.sent_quit = True

    def Deregister(self):
        """Removes a user from the global database.."""
        if self.registered:
            del self.server.users[self.nickname]
            if self in self.server.operators:
                self.server.operators.remove(self)
            self.registered = False

            self.server.whowas.AddEntry(self)

    def StopTimer(self):
        if not hasattr(self, "timeout_timer"):
            return

        if self.timeout_timer.active():
            self.timeout_timer.cancel()
        # This seems to be necessary to prevent circular references, otherwise
        # this object is never deleted.
        del self.timeout_timer

    def DropConnection(self):
        self.transport.loseConnection()

    def connectionLost(self, reason):
        if not self.dc_state:
            self.LogQuit()
            self.NotifyQuit("Remote closed the connection")
            self.Deregister()
            self.StopTimer()

    def ExcessFlood(self):
        self.LogQuit()
        self.SendError("Excess flood")
        self.NotifyQuit("Excess flood")
        self.Deregister()
        self.StopTimer()
        self.DropConnection()
