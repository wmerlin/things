# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

import twisted.python.reflect

Implemented = ["ADMIN", "AWAY", "INFO", "INVITE", "ISON", "JOIN", "KICK",
                "KILL", "KLINE", "LIST", "LUSERS", "MODE", "MOTD", "NAMES",
                "NICK", "NOTICE", "OPER", "PART", "PASS", "PING", "PONG",
                "PRIVMSG", "QUIT", "STATS", "TIME", "TOPIC", "USERHOST",
                "USER", "VERSION", "WALLOPS", "WHOIS", "WHO", "WHOWAS"]


Table = {}

def BuildTable():
    for command in Implemented:
        function = twisted.python.reflect.namedAny("pyircd.commands.%s.IRC_%s" %
                                                   (command.lower(), command))
        Table[command] = function
