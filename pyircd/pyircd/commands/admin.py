# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_ADMIN(self, args):
    if not self.server.admin_info:
        return self.ErrorCode("432", self.server.prefix, "No administrative info available")

    # According to the RFC, the server prefix should come in between. Other
    # servers don't seem to do this, though Freenode appends it to the message.
    self.ErrorCode("256", "Administrative info")

    # Unreal is a bit more creative with this, using all of 257, 258, and 259.
    # I don't see the point.
    for info in self.server.admin_info:
        self.ErrorCode("257", info)
