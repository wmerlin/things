# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_INVITE(self, args):
    if len(args) < 2:
        return self.NeedMoreParams("INVITE")
    nickname, target = args[0], args[1]

    user = self.server.users.get(nickname)
    if not user:
        return self.NoSuchNickOrChannel(nickname)

    channel = self.server.channels.get(target)
    if not channel:
        return self.NoSuchChannel(target)

    if not self.is_oper and target not in self.channels:
        return self.NotOnChannel(target)

    if nickname in channel.users:
        return self.ErrorCode("443", nickname, target, "is already on channel")

    if not self.is_oper and not channel.CanInvite(self):
        return

    user.SendFrom(self, "INVITE", self.nickname, target)

    # Weird. The RFC says that the order is <channel> <nick>, but everything
    # assumes the opposite order.
    self.ErrorCode("341", nickname, target)

    if user not in channel.invited:
        channel.invited.append(user)
    self.SendAwayNote(user)
