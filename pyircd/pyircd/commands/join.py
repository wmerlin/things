# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from pyircd.channel import Channel

def IRC_JOIN(self, args):
    if not len(args):
        return self.NeedMoreParams("JOIN")

    channels, keys = args[0].split(","), []
    if len(args) > 1:
        keys = args[1].split(",")
    keys.extend([None] * (len(channels) - len(keys)))

    for name, key in zip(channels, keys):
        if name[0] != "#":
            self.ErrorCode("403", name, "No such channel")
            continue

        # Ignore channels you're already on.
        if name in self.channels:
            continue

        if name in self.server.channels:
            # Join an existing channel
            channel = self.server.channels[name]
            if channel.CanJoin(self, key):
                channel.JoinUser(self)
            else:
                return
        else:
            # Check for invalid channel names
            if ":" in name or "\a" in name:
                self.ErrorCode("476", name, "Bad channel mask")
                continue

            # Create a new channel
            channel = Channel(self.server, name)
            channel.JoinInitialUser(self)
            self.server.channels[name] = channel

        self.channels[name] = channel
