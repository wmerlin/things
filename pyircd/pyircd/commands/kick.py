# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_KICK(self, args):
    if len(args) < 2:
        return self.NeedMoreParams("KICK")

    name = args[0]
    nickname = args[1]

    if len(args) > 2 and args[2]:
        comment = '"' + args[2] + '"'
    else:
        comment = self.nickname

    channel = self.server.channels.get(name)
    if not channel:
        return self.NoSuchChannel(name)

    if not self.is_oper:
        if name not in self.channels:
            return self.NotOnChannel(name)
        if self.nickname not in channel.chanops:
            return self.NotChanop(name)

    user = self.server.users.get(nickname)
    if not user:
        return self.NoSuchNickOrChannel(nickname)

    if name not in user.channels:
        return self.ErrorCode("441", nickname, name, "They aren't on that channel")

    channel.KickUser(self, user, comment)
    del user.channels[name]
