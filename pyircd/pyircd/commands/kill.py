# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from pyircd import log

def IRC_KILL(self, args):
    if len(args) < 2:
        return self.NeedMoreParams("KILL")

    if not self.is_oper:
        return self.NotOperator()

    nickname, comment = args[0], args[1]

    user = self.server.users.get(nickname)
    if not user:
        return self.NoSuchNickOrChannel(nickname)

    info = "%s (\"%s\")" % (self.nickname, comment)
    message = "Kill by %s" % info

    user.dc_state = True
    log.Log(log.Kill, "Killed connection by %s: %s (%s)" %
            (self.nickname, user.ip_address, user.prefix))
    user.SendFrom(self, "KILL", user.nickname, info)
    user.SendError(message)
    user.NotifyQuit(message)
    user.Deregister()
    user.StopTimer()
    user.DropConnection()
