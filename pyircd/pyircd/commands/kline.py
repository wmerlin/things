# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from pyircd import log, util

def IRC_KLINE(self, args):
    if not self.is_oper:
        return self.NotOperator()

    if not len(args):
        return self.NeedMoreParams("KLINE")

    target = args[0]

    if len(args) > 1:
        reason = " ".join(args[1:])
    else:
        reason = "No reason"

    if not target:
        # Empty parameter?
        return self.NoNicknameGiven()

    if target[0] != "-":
        if target[0] == "+" or target[0] == "-":
            target = target[1:]

        if not target:
            return self.NoNicknameGiven()

        if "@" in target:
            mask = target
        else:
            user = self.server.users.get(target)
            if not user:
                return self.NoSuchNickOrChannel(target)
            mask = "*@" + user.hostname

        klines = self.server.klines

        klines.append((util.IMask(mask), reason))
        self.FromServer("NOTICE", self.nickname, "Adding K-Line for %s (%s)" %
                        (mask, reason))
        log.Log(log.KLine, "Adding K-Line by %s (%s): %s (%s)" %
                (self.prefix, self.ip_address, mask, reason))

        users = [] # Can't delete users while iterating over them
        for user in self.server.users.values():
            mask = user.username + "@" + user.hostname
            for pattern, reason in klines:
                if pattern.match(mask):
                    users.append(user)
        for user in users:
            user.dc_state = True
            log.Log(log.KLine, "K-Lined connection from %s (%s): %s" %
                    (user.ip_address, user.prefix, reason))
            user.FromServer("NOTICE", user.nickname, "K-Lined by %s: %s" %
                            (self.nickname, reason))
            user.SendError("K-Lined: %s" % reason)
            user.NotifyQuit("K-Lined")
            user.Deregister()
            user.StopTimer()
            user.DropConnection()
    else:
        if target[0] == "+" or target[0] == "-":
            target = target[1:]

        if not target:
            return self.NoNicknameGiven()

        # Hack because IMask()'s are escaped
        pattern = util.IMask(target).pattern.lower()

        for idx, value in enumerate(self.server.klines):
            if value[0].pattern.lower() == pattern:
                log.Log(log.KLine, "Removing K-Line by %s (%s): %s (%s)" %
                        (self.prefix, self.ip_address, value[0].pattern, value[1]))
                self.server.klines.pop(idx)
                self.FromServer("NOTICE", self.nickname, "Removing K-Line for %s (%s)" %
                                (value[0].pattern, value[1]))
                return
