# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_LIST(self, args):
    if len(args):
        channels = args[0].split(",")
    else:
        channels = self.server.channels.keys()

    for name in channels:
        channel = self.server.channels.get(name)
        if channel:
            if channel.secret and not self.is_oper:
                continue

            if channel.topic:
                self.ErrorCode("322", name, str(len(channel.users)), channel.topic)
            else:
                self.ErrorCode("322", name, str(len(channel.users)), "")

    self.ErrorCode("323", "End of LIST")
