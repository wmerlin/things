# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def ChangeMode(self, args, target):
    # This is a fairly complicated command. Unlike user modes, it's
    # not nearly as easy to differentiate between a command to set
    # modes and one to request existing ones (it's even worse
    # because things like "+b" can be both at once).

    channel = self.server.channels.get(target)
    if not channel:
        return self.NoSuchNickOrChannel(target)

    # See if it's an obvious request (MODE #channel)
    if len(args) < 2:
        modes, args = channel.modes.State(self)
        return self.ErrorCode("324", target, modes, *args)

    # See if it's a request for a listable mode.
    if len(args[1]) < 3 and len(args) < 3:
        if self.nickname not in channel.users and not self.is_oper:
            return

        def _SendList(list, ncode, ecode, enote):
            for item in list.masks:
                self.ErrorCode(ncode, target, item)
            self.ErrorCode(ecode, target, enote)

        # The string can either be "b" or "+b", so -1. Fortunately you
        # can't request more than one at once.
        char = args[1][-1]
        if char == "I":
            return _SendList(channel.invite_ex, "345", "347", "End of channel invite list")
        elif char == "b":
            return _SendList(channel.bans, "367", "368", "End of channel ban list")
        elif char == "e":
            return _SendList(channel.ban_ex, "348", "349", "End of channel exception list")

    # At this point, it's not a request of any kind. You'd better have ops.
    if self.nickname not in channel.chanops and not self.is_oper:
        return self.NotChanop(target)

    modes, args = channel.modes.Process(self, args[1], args[2:])
    if modes:
        channel.SendToAll(self, "MODE", target, modes, *args)
