# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def ChangeMode(self, args, target):
    user = self.server.users.get(target)
    if not user:
        return self.ErrorCode("401", target, "No such nick/channel")

    if target.lower() != self.nickname.lower():
        if not self.is_oper:
            if len(args) > 1:
                self.ErrorCode("502", "Cannot change mode for other users")
            return

    if len(args) > 1:
        modes, args = user.modes.Process(self, args[1], [])
        if modes:
            user.SendFrom(self, "MODE", target, modes)
            if user != self:
                self.FromSelf("MODE", target, modes)
    else:
        self.ErrorCode("221", user.modes.State(self)[0])
