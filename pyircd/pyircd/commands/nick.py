# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

import string

# Valid nickname characters.
ValidNickStartChars = string.ascii_letters + "[]\`_^{|}"
ValidNickChars = ValidNickStartChars + string.digits + "-"

def IRC_NICK(self, args):
    if not len(args):
        return self.NoNicknameGiven()
    old, nickname = self.nickname, args[0]

    # Check validity
    if not nickname:
        return self.ErrorCode("432", nickname, "Erroneous nickname: Empty parameter")
    if nickname[0] not in ValidNickStartChars:
        return self.ErrorCode("432", nickname, "Erroneous nickname: Illegal characters")
    for char in nickname:
        if char not in ValidNickChars:
            return self.ErrorCode("432", nickname, "Erroneous nickname: Illegal characters")

    # Ignore it if it's the same.
    if self.nickname and (nickname.lower() == self.nickname.lower()):
        return

    # Check against Q-Lines.
    for pattern, reason in self.server.qlines:
        if pattern.match(nickname):
            return self.ErrorCode("432", nickname, "Erroneous nickname: %s" % reason)

    # If it exceeds length limits, chop off the end.
    nickname = nickname[:self.server.max_nick_length]

    # Check that it's not already in use
    if nickname in self.server.users:
        return self.ErrorCode("433", nickname, "Nickname is already in use")

    if self.registered:
        self.server.users[nickname] = self
        del self.server.users[old]
        self.nickname = nickname
        self.FromSelf("NICK", nickname)
        for channel in self.channels.values():
            channel.NotifyNickChange(self, old, nickname)
        self._UpdatePrefix()
    else:
        self.nickname = nickname
        self.MaybeRegister()
