# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from pyircd import log

def _Fail(self, reason):
    log.Log(log.Operator, "Failed operator authentication from %s (%s): %s" %
            (self.ip_address, self.prefix, reason))

def IRC_OPER(self, args):
    if len(args) < 2:
        return self.NeedMoreParams("OPER")

    username, password = args[0], args[1]

    if username not in self.server.oper_auth:
        _Fail(self, "Unknown username %s" % username)
        return self.ErrorCode("491", "No O-lines for your host")

    if password != self.server.oper_auth[username]:
        _Fail(self, "Bad password for %s" % username)
        return self.ErrorCode("464", "Password incorrect")

    if not self.is_oper:
        self.server.operators.append(self)
        log.Log(log.Operator, "Operator authenticated to %s from %s (%s)" %
                (username, self.ip_address, self.prefix))

    self.is_oper.state = True
    self.FromSelf("MODE", self.nickname, "+o")
    self.ErrorCode("381", "You are now an IRC operator")
