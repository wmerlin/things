# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_PART(self, args):
    if not len(args):
        return self.NeedMoreParams("PART")

    channels = args[0].split(",")
    message = '"' + args[1] + '"' if len(args) > 1 else None

    for name in channels:
        if name not in self.server.channels:
            self.NoSuchChannel(name)
            continue
        if name not in self.channels:
            self.NotOnChannel(name)
            continue

        self.channels[name].PartUser(self, message)
        del self.channels[name]
