# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def _Privmsg(self, args, command, is_notice=False):
    """Since PRIVMSG and NOTICE differ only in the name of the command, this
    function just does both."""
    if not len(args):
        return self.ErrorCode("411", "No recipient given (%s)" % command)
    if len(args) < 2:
        return self.ErrorCode("412", "No text to send")
    target, message = args[0], args[1]

    if target[0] == "#":
        channel = self.server.channels.get(target)
        if not channel:
            return self.NoSuchNickOrChannel(target)

        if self.is_oper:
            channel.SendMessage(self, command, message)
        elif channel.CanSend(self, message):
            if is_notice and channel.no_notices and self.nickname not in channel.chanops:
                return self.ErrorCode("404",  target, "NOTICEs are not permitted in this channel")

            channel.SendMessage(self, command, message)
    else:
        user = self.server.users.get(target)
        if not user:
            return self.NoSuchNickOrChannel(target)
        user.SendFrom(self, command, target, message)
        self.SendAwayNote(user)

def IRC_PRIVMSG(self, args):
    _Privmsg(self, args, "PRIVMSG")
