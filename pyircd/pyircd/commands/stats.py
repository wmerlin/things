# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

import datetime

def IRC_STATS(self, args):
    Stat = self.ErrorCode

    if not len(args) or len(args[0]) > 1:
        return self.ErrorCode("219", "*", "End of /STATS report")

    char = args[0]

    if char == "u":
        delta = datetime.datetime.now() - self.server.creation_time
        tmp = delta.seconds

        seconds = tmp % 60
        tmp /= 60

        minutes = tmp % 60
        tmp /= 60

        hours = tmp % 24
        tmp /= 24

        Stat("242", "Server up %s days %02i:%02i:%02i" % (delta.days, hours, minutes, seconds))
    elif char == "m":
        for command, uses in self.server.command_stats.items():
            Stat("212", command, str(uses))

    self.ErrorCode("219", char, "End of /STATS report")
