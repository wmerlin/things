# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_TOPIC(self, args):
    if not len(args):
        return self.NeedMoreParams("TOPIC")
    name = args[0]

    channel = self.server.channels.get(name)
    if not channel:
        return self.NoSuchChannel(name)

    if len(args) > 1:
        if name not in self.channels and not self.is_oper:
            return self.NotOnChannel(name)

        to = args[1]

        if channel.op_only_topic and self.nickname not in channel.chanops and not self.is_oper:
            return self.NotChanop(name)

        channel.SetTopic(self, to)
    else:
        channel.SendTopic(self)
