# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_USER(self, args):
    if self.registered:
        return self.ErrorCode("462", "You may not reregister")
    if len(args) < 4:
        return self.NeedMoreParams("USER")

    username = args[0]
    # All chars are valid in a username except "@". We simply strip off
    # anything after one.
    if "@" in username:
        username = username[:username.find("@")]
    self.username = username
    self.realname = args[3]

    self.MaybeRegister()

    if not self.registered:
        return

    try:
        mask = int(args[1])
    except ValueError:
        return

    acc = "+"
    if mask & (1 << 2):
        acc += "w"
    elif mask & (1 << 3):
        acc += "i"

    modes, args = self.modes.Process(self, acc, [])
    if modes:
        self.FromSelf("MODE", self.nickname, modes)
