# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

# Todo: Add support for optional second parameter, to match only operators.
# Also todo: Add support for masks and "0" (all users).

def IRC_WHO(self, args):
    if len(args) and args[0][0] == "#":
        name = args[0]
        channel = self.server.channels.get(name)

        if channel:
            # Do not show secret channels.
            if channel.secret and not self.nickname in channel.users and not self.is_oper:
                return self.ErrorCode("315", name, "End of /WHO list")

            for nickname, user in channel.users.items():
                # Do not send invisible users, unless you're on the channel.
                if user.is_invisible and not self.nickname in channel.users and not self.is_oper:
                    continue

                if user.is_away:
                    mode = "G"
                else:
                    mode = "H"

                if user.is_oper:
                    mode += "*"

                if nickname in channel.chanops:
                    mode += "@"
                elif nickname in channel.voiced:
                    mode += "+"

                self.ErrorCode("352", name, user.username, user.hostname,
                               self.server.prefix, user.nickname, mode,
                               "0 " + user.realname)

        self.ErrorCode("315", name, "End of /WHO list")
    else:
        self.ErrorCode("315", "*", "End of /WHO lsit")
