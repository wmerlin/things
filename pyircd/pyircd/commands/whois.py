# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_WHOIS(self, args):
    if not len(args):
        return self.NoNicknameGiven()

    nickname = args[0]

    user = self.server.users.get(nickname)
    if not user:
        self.NoSuchNickOrChannel(nickname)
        return self.ErrorCode("318", nickname, "End of /WHOIS list")

    self.ErrorCode("311", user.nickname, user.username, user.hostname, "*", user.realname)
    self.ErrorCode("312", user.nickname, self.server.prefix, self.server.network)

    if user.is_oper:
        self.ErrorCode("313", user.nickname, "is an IRC operator")

    acc = ""
    for name, channel in user.channels.items():
        # +i user mode
        if user.is_invisible and name not in self.channels and not self.is_oper:
            continue

        # +s channel mode
        if channel.secret and self.nickname not in channel.users and not self.is_oper:
            continue

        if user.nickname in channel.chanops:
            acc += "@"
        elif user.nickname in channel.voiced:
            acc += "+"
        acc += name + " "

    if acc:
        self.ErrorCode("319", user.nickname, acc)

    # Todo: 318 (idle time)

    self.SendAwayNote(user)
    self.ErrorCode("318", user.nickname, "End of /WHOIS list")
