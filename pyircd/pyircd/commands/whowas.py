# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

def IRC_WHOWAS(self, args):
    if not len(args):
        return self.NoNicknameGiven()

    nickname = "*"

    for nickname in args[0].split(","):
        info = self.server.whowas.Find(nickname)
        if info:
            self.ErrorCode("314", nickname, info[1], info[2], "*", info[3])
        else:
            self.ErrorCode("406", nickname, "There was no such nickname")

    self.ErrorCode("369", nickname, "End of WHOWAS")
