# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

Sent          = 1      # Logs sent data.
Received      = 1 << 1 # Logs received data.
Connection    = 1 << 2 # Logs connections.
Disconnection = 1 << 3 # Logs disconnections.
Crash         = 1 << 4 # Logs server crashes (tracebacks are written in any case).
Operator      = 1 << 5 # Logs usage of /oper (both successful and failed).
# Logs connections that are dropped from Z-Lines, K-Lines, etc.
BanKill       = 1 << 6
Kill          = 1 << 7 # Logs usage of /kill.
KLine         = 1 << 8 # Logs usage of /kline.

_Log = 0

_LogTypes = {
    "sent": Sent,
    "received": Received,
    "connect": Connection,
    "disconnect": Disconnection,
    "crash": Crash,
    "operator": Operator,
    "bankill": BanKill,
    "kill": Kill,
    "kline": KLine
    }

def Configure(config):
    global _Log

    for item in config.get("Logging", []):
        mask = _LogTypes.get(item.lower())
        if mask is None:
            print "Warning: Unknown log type \"%s\"" % item
            continue
        _Log |= mask

def Log(type, *args):
    if _Log & type:
        print " ".join(args)
