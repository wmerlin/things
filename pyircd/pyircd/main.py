# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from twisted.application.service import MultiService
from twisted.internet import defer
from twisted.internet.error import CannotListenError
from twisted.internet.protocol import ServerFactory

import pyircd.client
import pyircd.commands
import pyircd.log
import pyircd.server
import pyircd.util

class ClientFactory(ServerFactory):
    """Client factory."""

    def __init__(self, server):
        self.server = server

    def buildProtocol(self, address):
        return pyircd.client.Client(self.server, address)

class PyIRCdService(MultiService):
    def __init__(self, config):
        MultiService.__init__(self)
        self.config = config
        self.server = pyircd.server.IrcServer(self.config)
        self.factory = ClientFactory(self.server)
        self.ports = []
        pyircd.log.Configure(self.config)

    def startService(self):
        from twisted.internet import reactor
        MultiService.startService(self)
        for port in self.config["Ports"]:
            new_port = reactor.listenTCP(port, self.factory)
            self.ports.append(new_port)

    def stopService(self):
        ms_deferred = MultiService.stopService(self)
        deferreds = []
        for port in self.ports:
            deferred = port.stopListening()
            deferreds.append(deferred)
        self.ports = []
        return defer.DeferredList(deferreds + [ms_deferred])
