# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################


import twisted.conch.manhole_tap

def EnableManhole(service, config):
    namespace = {
        "service": service,
        "server": service.server,
        "channels": service.server.channels,
        "users": service.server.users
        }
    manhole = twisted.conch.manhole_tap.makeService({
        "sshPort": str(config["Manhole port"]),
        "telnetPort": None,
        "passwd": "manhole.auth",
        "namespace": namespace
        })
    manhole.setServiceParent(service)
