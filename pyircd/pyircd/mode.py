# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

"""PyIRCd's mode system.

The MODE command is a very flexible one, and it serves many purposes at once.
To make it worse, many of them have different syntaxes. Some behave differently
depending on whether you're adding or removing modes.

Early versions were long chains of if/elif/else that were repeated several
times over. While this worked, it was ugly and annoying to maintain. Our
current system, while somewhat complicated on the face, has a reasonably simple
API.

Every mode is represented by an instance of a class that derives from Mode. A
set of modes (on a user, or a channel, etc.) is represented by an instance of
Modes, which knows about all of them.

The Updates class represents the set of changes over the course of the entire
MODE command, so that duplicates or modes that cancel each other out can be
eliminated and not sent to clients.

In fact, it is partially because of the update system that this becomes as
complicated as it is. There is no fits-all means of representing changes, so
this is left to the Mode class to interpret. This is done by means of two
methods:

    Default(). This is called to get the information needed to represent
    changes with this mode. It can be anything, it has no meaning to the rest
    of the system.

    Interpret(). This is called to convert the data that Default() returns into
    something more concrete for transport to clients.

Make note of the fact that the data returned from Default() is passed around
directly, so it must be of a complex type. I.e. you can't return 'True', but
you can return [True].

Modes are manipulated by means of the Process() function, which must be
implemented in subclasses.
"""

class Mode(object):
    """A mode that can be set and unset.

    A mode has only one property:

        char: The character that triggers this mode."""

    def __init__(self, char):
        self.char = char

    def Process(self, user, state, data, args):
        """Called to implement a mode.

        @arg user: The user that initiated the changes.
        @arg state: Whether to set or unset the flag (+ or -).
        @arg data: As you define.
        @arg args: The remaining arguments."""
        raise NotImplementedError

    def State(self, user):
        """Return the current state of this mode.

        @arg user: The user requesting the mode.
        @return: A tuple. (added, args)."""
        return "", []

    def Default(self):
        """Return the initial value of whatever data this mode uses to track
        changes."""
        raise NotImplementedError

    def Interpret(self, data):
        """Transform the data this mode uses into something that can be sent
        to the client.

        @return: A tuple. (added, removed, args)."""
        raise NotImplementedError

class Flag(Mode):
    """A boolean flag."""

    def __init__(self, char, initial=False):
        """Constructor.

        @arg char: The character that triggers this mode.
        @arg initial: The initial value for the mode."""
        Mode.__init__(self, char)
        self.state = initial

    def Process(self, user, state, data, args):
        if state:
            self.Set(user, data)
        else:
            self.Unset(user, data)

    def Set(self, user, data):
        """Sets the flag."""
        self.state = True
        data[1] = True

    def Unset(self, user, data):
        """Unsets the flag."""
        self.state = False
        data[1] = False

    def State(self, user):
        if self.state:
            return self.char, []
        else:
            return "", []

    def Default(self):
        # Original, new
        return [self.state, None]

    def Interpret(self, data):
        if data[0] != data[1]:
            if data[1] is True:
                return self.char, "", []
            elif data[1] is False:
                return "", self.char, []
        return "", "", []

    def __nonzero__(self):
        """Convenience function to treat this object as a bool."""
        return self.state

class ParamMode(Mode):
    """A mode that takes parameters when being modified."""

    def Process(self, user, state, data, args):
        if not args:
            return
        arg = args.pop(0)

        if state:
            self.Add(user, data, arg)
        else:
            self.Remove(user, data, arg)

    def Add(self, user, data, arg):
        raise NotImplementedError

    def Remove(self, user, data, arg):
        raise NotImplementedError

class Updates(object):
    """A class encapsulating overall mode changes.

    This is so we can neatly keep track of which modes have actually been
    changed, and which are redundant, or cancel each other out.

    Its methods mostly correspond to Mode."""

    def __init__(self):
        # char -> (mode, data)
        self.updates = {}

    def Create(self, mode):
        if mode.char not in self.updates:
            self.updates[mode.char] = [mode, mode.Default()]

    def Results(self):
        """Calculate the overall changes.

        @return: A tuple of (changes, args).

                 "changes" is a string of mode arguments.
                 "args" is a list of arguments.

        The overall value of this function is something that would be adequate
        to pass to a MODE reply."""
        added, removed, args = "", "", []

        for mode, data in self.updates.values():
            tmp = mode.Interpret(data)
            added += tmp[0]
            removed += tmp[1]
            args.extend(tmp[2])

        acc = ""
        if added:
            acc += "+" + added
        if removed:
            acc += "-" + removed

        return acc, args

class Modes(object):
    """A set of modes. There should be one of these objects for each instance
    of a class that accepts mode changes.

    Note that this class cannot be instantiated directly; you must inherit and
    implement OnUnknown(). This is because the behavior of that function is
    usually different for each object, so a generic function that can suit more
    than one scenario is not possible."""

    def __init__(self, **modes):
        """Constructor.

        Usage:

            modes = Modes(t=some_Flag_instance, x=something_else, ...)"""
        self.modes = modes

    def Parse(self, user, modes, args):
        """Parse mode changes.

        @arg user: The user initiating the changes.
        @arg modes: The mode string. E.g. +stnb
        @arg args: Arguments to the MODE command. E.g. ["*!*@*"]

        Returns an Updates object."""
        state = True
        updates = Updates()

        for char in modes:
            if char == "+":
                state = True
            elif char == "-":
                state = False
            elif char in self.modes:
                mode = self.modes[char]
                updates.Create(mode)
                mode.Process(user, state, updates.updates[char][1], args)
            else:
                self.OnUnknown(user, char)

        return updates

    def Process(self, user, modes, args):
        """A light wrapper around Parse() that additionally calls Results() on
        the update object that Parse() returns, since this is what you usually
        want.

        See Parse() for usage."""
        updates = self.Parse(user, modes, args)
        return updates.Results()

    def OnUnknown(self, user, char):
        """Override this function to define what happens when an unknown mode
        character is encountered."""
        raise NotImplementedError

    def State(self, user):
        """Get the list of currently set modes.

        @arg user: The user requesting the modes.
        @return: (modes, args). modes is a mode string, args a list. Together
                 they are suitable for sending directly to the client."""
        modes, args = "+", []
        for mode in self.modes.values():
            tmp = mode.State(user)
            modes += tmp[0]
            args.extend(tmp[1])
        return modes, args
