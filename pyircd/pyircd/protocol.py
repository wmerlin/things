# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from twisted.internet.protocol import Protocol
from pyircd import log

def Parse(line):
    """Parse an incoming IRC message.

    Invalid messages are simply discarded, and None returned. Otherwise this
    will return a tuple, (prefix, command, args) as defined by the RFC."""
    if not line.strip():
        # "", " ", "  ", ...
        return

    if line[0] == ":":
        line = line[1:]
        if not line:
            # ":"
            return

        index = line.find(" ")
        if index == -1:
            # ":foo"
            return

        prefix = line[:index]
        line = line[index+1:].lstrip()

        if not line:
            # ":foo "
            return
    else:
        prefix = None

    if " :" in line:
        line, trailing = line.split(" :", 1)
        trailing = [trailing]
    else:
        trailing = []

    args = line.split() + trailing
    return prefix, args[0], args[1:]

class Protocol(Protocol):
    """Generic protocol wrapper.

    This encapsulates most of the IRC protocol, both received and sent, and
    various convenience functions are included.

    Even though the client protocol and server protocol differ greatly in
    semantics, the syntax is the same, so we put the generic parts of it here.
    If PyIRCd ever becomes capable of server links, this class should hopefully
    still work.

    Unfortunately it is not quite ready- it assumes in several places that
    Client has inherited from it, and as such will access instance variables
    of Client."""

    def __init__(self, server):
        self.server = server

    ##########################################################################
    ## Incoming data

    buffer = ""
    MAX_LENGTH = 512

    def dataReceived(self, data):
        # This is mostly stolen from Twisted's LineOnlyReceiver, with two
        # additions:
        #   - It imposes a limit on the size of the buffer.
        #   - It supports both \r\n and \n.

        self.buffer += data
        if len(self.buffer) > self.server.recvq_limit:
            # Fixme: Going up the inheritance hierarchy again.
            return self.ExcessFlood()

        lines = self.buffer.split("\n")
        self.buffer = lines.pop()

        for line in lines:
            # Twisted does this for some reason, so we do as well.
            if self.transport.disconnecting:
                return
            if not line:
                continue
            if line[-1] == "\r":
                line = line[:-1]
            if len(line) > self.MAX_LENGTH:
                self.lineReceived(line[:self.MAX_LENGTH])
            else:
                self.lineReceived(line)

        if len(self.buffer) > self.MAX_LENGTH:
            self.lineReceived(self.buffer[:self.MAX_LENGTH])
            self.buffer = ""

    def lineReceived(self, line):
        tmp = Parse(line)
        if tmp is None:
            return
        prefix, command, args = tmp

        self.ProcessCommand(command, args)

    def ProcessCommand(self, command, args):
        raise NotImplementedError

    ##########################################################################
    ## Outgoing data

    def SendLine(self, line):
        """Sends a line to the other end.

        Currently just a thin wrapper that prints the line before actually
        sending it, for debugging purposes. This is probably not necessary
        anymore."""
        log.Log(log.Sent, "Sending:", line)
        self.transport.write(line + "\r\n")

    def Send(self, prefix, command, *args):
        """Low-level send function.

        It is not usually necessary, nor is it preferable, to call this
        function.

        @arg prefix: According to the RFC, the source of the message.
        @arg command: The command.

        Remaining arguments are treated as parameters to the command. The last
        one is escaped into a trailing paramter if it appears necessary."""
        args = list(args)
        if args:
            # It would be nice if we could set args[-1] to be trailing parameter
            # unconditionally, but apparently some clients don't like things
            # such as:
            #     :Foo!foo@example.com MODE #channel +v :Bar
            # Requiring instead:
            #     :Foo!foo@example.com MODE #channel +v Bar
            #
            # Also, the last condition is something of a hack to support the
            # empty parameter that TOPIC and ISON will sometimes send.
            if " " in args[-1] or ":" in args[-1] or args[-1] == "":
                args[-1] = ":" + args[-1]
        self.SendLine(":%s %s %s" % (prefix, command, " ".join(args)))

    def FromServer(self, command, *args):
        """Send a message originating from this server."""
        self.Send(self.server.prefix, command, *args)

    def SendFrom(self, user, command, *args):
        """Send a message originating from another user."""
        self.Send(user.prefix, command, *args)

    def FromSelf(self, command, *args):
        """Send a message originating from this user."""
        self.Send(self.prefix, command, *args)

    def ErrorCode(self, code, *args):
        """Send an error code."""
        self.Send(self.server.prefix, code, self.nickname, *args)
