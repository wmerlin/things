# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

import datetime
import time
import pyircd.util
import pyircd.whowas

class IrcServer(object):
    """Server object.

    All global state and configuration goes here."""

    PyIRCdVersion = "PyIRCd-R2"
    AvailUserModes = "iow"
    AvailChannelModes = "bCeiIklmnostTv"

    def __init__(self, config):
        # The choice of naming here conveniently makes this object somewhat
        # compatible with a client object.
        serverinfo = config["Server info"]
        self.network = serverinfo["Network"]
        self.prefix = serverinfo["Hostname"]

        self.creation_time = datetime.datetime.now()
        self.creation_timestamp = time.strftime("%c")

        self.users = pyircd.util.IDict()
        self.channels = pyircd.util.IDict()

        self.whowas = pyircd.whowas.WhowasList(config)
        # command -> uses
        self.command_stats = {}

        self.operators = []

        self.ping_interval = config.get("Ping interval", 60)
        self.recvq_limit = config.get("Receive queue limit", 8192)
        # Todo: sendq limit.

        def _Line(name):
            dict = config.get(name, [])
            if dict is None:
                return []
            return [(pyircd.util.IMask(pattern), reason) for pattern, reason in dict.items()]

        self.qlines = _Line("Banned nicknames")
        self.zlines = _Line("Banned IPs")
        self.klines = _Line("Banned hostnames")
        self.nlines = _Line("Banned names")

        self.oper_auth = config["Operators"]

        self.max_nick_length = config.get("Maximum nickname length", 16)
        self.def_user_modes = config.get("Default user modes", "")

        tmp = config.get("Default channel modes", "").split()
        self.def_channel_modes = tmp[0] if tmp else ""
        self.def_channel_args = tmp[1:]

        self.admin_info = config.get("Admin info", "").strip("\n").split("\n")

        self.has_motd = False
        motd_filename = config.get("MOTD filename", None)
        if motd_filename:
            try:
                file = open(motd_filename)
            except IOError:
                print "Warning: Cannot open MOTD File (%s)" % motd_filename
            else:
                self.motd = [line.rstrip() for line in file]
                self.has_motd = True
