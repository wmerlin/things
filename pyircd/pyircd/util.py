# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

"""Various utilities."""

import re
import string

class IDict(object):
    """A dictionary with case-insensitive keys."""

    def __init__(self, initial={}):
        self.dict = {}
        for key, value in initial.items():
            self.dict[key.lower()] = (key, value)
        self.__len__ = self.dict.__len__

    def keys(self):
        for item in self.dict.itervalues():
            yield item[0]

    __iter__ = keys

    def values(self):
        for item in self.dict.itervalues():
            yield item[1]

    def items(self):
        for item in self.dict.itervalues():
            yield item[0], item[1]

    def __delitem__(self, key):
        del self.dict[key.lower()]

    def __contains__(self, key):
        return key.lower() in self.dict

    def __getitem__(self, key):
        return self.dict[key.lower()][1]

    def __setitem__(self, key, value):
        self.dict[key.lower()] = (key, value)

    def __len__(self):
        return len(self.dict)

    def get(self, key, default=None):
        try:
            return self.dict[key.lower()][1]
        except KeyError:
            return default

_Escaped = "^$*+?{}[]|()\\"
_NonEscaped = string.ascii_lowercase + string.ascii_uppercase + string.digits
def IMask(mask):
    """Escape a regular expression and compile it to be case-insensitive.

    Returns the compiled regexp."""

    # The code is more or less stolen from re.escape(). Todo: Create our own
    # regexp library, with no more than we need.
    chars = list(mask)
    for i, char in enumerate(mask):
        if char == "*":
            chars[i] = ".*"
        elif char in _Escaped:
            chars[i] = "\\" + char
    return re.compile("".join(chars), re.IGNORECASE)
