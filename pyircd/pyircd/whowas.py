# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

class WhowasList(object):
    """WHOWAS database."""

    def __init__(self, config):
        self.size = config.get("WHOWAS size", 1000)
        # Todo: This is wasteful. Should grow the array as needed.
        self.entries = [None] * self.size
        self.pos = 0

    def AddEntry(self, user):
        """Insert an entry into the database.

        @arg user: The Client object."""

        # Todo: Remove old entries to save space.
        info = [user.nickname.lower(), user.username, user.hostname, user.realname]
        self.entries[self.pos] = info
        self.pos = (self.pos + 1) % self.size

    def Find(self, nickname):
        """Lookup a nickname.

        @return: If the nickname was found, a four part list:

            info[0]: The nickname.
            info[1]: The username.
            info[2]: The hostname.
            info[3]: The real name.

        Otherwise None."""
        nickname = nickname.lower()

        pos = self.pos
        while pos < self.size:
            info = self.entries[pos]
            if info and info[0] == nickname:
                return info
            pos += 1
        pos = 0
        while pos < self.pos:
            info = self.entries[pos]
            if info and info[0] == nickname:
                return info
            pos += 1
