#!/usr/bin/env python
# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from distutils.core import setup

setup(name="PyIRCd", version="pre-release",
      author="Wade Sowders", author_email="wadesowders+pyircd@gmail.com",
      maintainer="Wade Sowders", maintainer_email="wadesowders+pyircd@gmail.com",
      license="GNU General Public License (GPL), v3",
      description="A Python IRC daemon.",
      packages=["pyircd", "pyircd.commands", "pyircd.commands.mode",
                "pyircd.channel", "twisted.plugins"],
      classifiers=[
          "Development Status :: 3 - Alpha",
          "Environment :: No Input/Output  (Daemon)",
          "Intended Audience :: System Administrators",
          "License :: OSI Approved :: GNU General Public License (GPL)",
          "Operating System :: OS Independent",
          "Programming Language :: Python",
          "Topic :: Communications :: Chat :: Internet Relay Chat"
      ])
