# -*- coding: utf-8 -*-
################################################################################
##                      Copyright (C) 2013, Wade Sowders                      ##
##                        This file is part of PyIRCd.                        ##
##                                                                            ##
##  PyIRCd is free software: you can redistribute it and/or modify it under   ##
##  the terms of the GNU General Public License as published by the Free      ##
##  Software Foundation, either version 3 of the License, or (at your         ##
##  option) any later version.                                                ##
##                                                                            ##
##  PyIRCd is distributed in the hope that it will be useful, but WITHOUT     ##
##  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     ##
##  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License      ##
##  for more details.                                                         ##
##                                                                            ##
##  You should have received a copy of the GNU General Public License along   ##
##  with PyIRCd. If not, see <http://www.gnu.org/licenses/>.                  ##
################################################################################

from zope.interface import implements

from twisted.python import usage
from twisted.plugin import IPlugin
from twisted.application.service import IServiceMaker

import pyircd.main
import pyircd.manhole

import os
import os.path
import yaml

class Options(usage.Options):
    optParameters = [["config", "c", "pyircd.cfg", "The configuration file"]]

    def postOptions(self):
        self.filename = self["config"]
        try:
            file = open(self.filename)
        except IOError, error:
            raise usage.UsageError("Cannot open %s: %s" % (self.filename, error))
        try:
            self.config = yaml.load(file)
        except yaml.YAMLError, error:
            raise usage.UsageError("YAML error: %s" % error)

class PyIRCdServiceMaker(object):
    implements(IServiceMaker, IPlugin)
    tapname = "pyircd"
    description = "A Python IRC daemon."
    options = Options

    def makeService(self, options):
        dirname = os.path.dirname(options.filename)
        if dirname:
            os.chdir(dirname)
        config = options.config
        service = pyircd.main.PyIRCdService(config)
        if config.get("Enable manhole", False):
            pyircd.manhole.EnableManhole(service, config)
        return service

serviceMaker = PyIRCdServiceMaker()
