An clone of an old browser game called OGame, written in Python with Django.

Though it was never fully completed (one or two important functions were left
out), this was my first significant webapp.

The game world is supposed to be able to support populations of several
thousand players on the same instance, and certain stats update in seeming
real-time, making the naive approach to maintaining resource levels (updating
every second) extremely poor. Though I can only guess as to how the "real"
version did it, my solution was to be lazy about nearly everything the user was
able to do- almost nothing updates until absolutely necessary. In the meantime,
the database holds queues of pending operations and their starting times, which
are flushed when the user views the appropriate page. (This also implicitly
"improved" significantly on the original game, giving the player great
flexibility in how to plan certain actions- although this was most likely left
out of the original intentionally, as a subset of this feature was only
available for pay.)

----

The code was originally written against Django 1.1, which by now is quite
outdated, and I suspect it would not run unmodified anymore (as I recall, due
to a lack of CSRF protection). Nevertheless, if you were to attempt to, you
would need at a minimum:

    python-django
    python-jinja2
    python-django-registration

and to modify the hardcoded paths in settings.py before doing anything else.
