# -*- coding: utf-8 -*-

from django.contrib import admin
from sgame import models

class UserAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("user",)}),
        )
    list_display = ("user",)

class PlanetAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("user", "name")}),
        ("Temperature", {"fields": ("maximum_temperature", "minimum_temperature")}),
        ("Location", {"fields": ("galaxy", "system", "slot")}),
        )
    list_display = ("name", "user", "galaxy", "system", "slot")

class ResourcesAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("planet", "last_update")}),
        ("Resources", {"fields": ("metal", "crystal", "deuterium")}),
        ("Power factors", {"fields": (
            "metal_mine_power_factor", "crystal_mine_power_factor",
            "deuterium_synthesizer_power_factor",
            "solar_plant_power_factor", "fusion_reactor_power_factor",)}),
        )
    list_display = ("planet", "metal", "crystal", "deuterium")

class BuildingsAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("planet",)}),
        ("Production", {"fields": (
            "metal_mine_level", "crystal_mine_level", "deuterium_synthesizer_level")}),
        ("Storage", {"fields": (
            "metal_storage_level", "crystal_storage_level", "deuterium_tank_level")}),
        ("Power", {"fields": ("solar_plant_level", "fusion_reactor_level")}),
        ("Facilities", {"fields": (
            "robotics_factory_level", "shipyard_level", "research_lab_level",
            "alliance_depot_level", "missile_silo_level", "nanite_factory_level",
            "terraformer_level")})
        )
    list_display = ("planet", "metal_mine_level", "crystal_mine_level",
        "deuterium_synthesizer_level", "solar_plant_level", "fusion_reactor_level",
        "robotics_factory_level", "shipyard_level", "research_lab_level",
        "nanite_factory_level")

class TechnologyAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("user",)}),
        ("Basic", {"fields": (
            "energy_tech_level", "laser_tech_level", "ion_tech_level",
            "hyperspace_tech_level", "plasma_tech_level")}),
        ("Drives", {"fields": (
            "combustion_drive_level", "impulse_drive_level", "hyperspace_drive_level")}),
        ("Advanced", {"fields": (
            "espionage_tech_level", "computer_tech_level", "astrophysics_level",
            "intergalactic_research_net_level", "graviton_tech_level")}),
        ("Combat", {"fields": (
            "armour_tech_level", "weapons_tech_level", "shielding_tech_level")}),
        )
    list_display = ("user",)

    # Just too long!
#    list_display = ("user", "energy_tech_level", "laser_tech_level", "ion_tech_level",
#        "hyperspace_tech_level", "plasma_tech_level", "combustion_drive_level",
#        "impulse_drive_level", "hyperspace_drive_level", "espionage_tech_level",
#        "computer_tech_level", "astrophysics_level", "intergalactic_research_net_level",
#        "graviton_tech_level", "armour_tech_level", "weapons_tech_level", "shielding_tech_level")

class ShipsAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("planet",)}),
        ("Utility ships", {"fields": (
            "small_cargo_ship_count", "large_cargo_ship_count",
            "recycler_count", "colony_ship_count")}),
        ("Light warships", {"fields": (
            "light_fighter_count", "heavy_fighter_count", "cruiser_count")}),
        ("Heavy warships", {"fields": (
            "battleship_count", "battlecruiser_count", "destroyer_count",
            "bomber_count")}),
        ("Other", {"fields": (
            "death_star_count", "espionage_probe_count", "solar_satellite_count")}),
        )
    # This is too long...
    list_display = ("planet", "small_cargo_ship_count", "large_cargo_ship_count",
        "recycler_count", "colony_ship_count", "light_fighter_count",
        "heavy_fighter_count", "cruiser_count", "battleship_count",
        "battlecruiser_count", "destroyer_count", "bomber_count",
        "death_star_count", "espionage_probe_count", "solar_satellite_count")

class DefensesAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("planet",)}),
        ("Light defense", {"fields": (
            "rocket_launcher_count", "light_laser_count")}),
        ("Medium defense", {"fields": (
            "heavy_laser_count", "ion_cannon_count")}),
        ("Heavy defense", {"fields": (
            "gauss_cannon_count", "plasma_turret_count")}),
        ("Shield domes", {"fields": (
            "small_shield_dome_count", "large_shield_dome_count")}),
        )
    list_display = ("planet", "rocket_launcher_count", "light_laser_count",
        "heavy_laser_count", "ion_cannon_count", "gauss_cannon_count",
        "plasma_turret_count", "small_shield_dome_count", "large_shield_dome_count")

class QueueAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("user", "planet", "subqueue_name")}),
        ("Status", {"fields": ("start_time", "end_time", "started")}),
        )
    list_display = ("user", "planet", "subqueue_name", "start_time", "end_time", "started")

class BuildingQueueAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("parent", "name", "target_level", "deconstructing")}),
        ("Cost", {"fields": ("metal_cost", "crystal_cost", "deuterium_cost")}),
        )
    list_display = ("name", "target_level")

class ResearchQueueAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("parent", "name", "target_level")}),
        ("Cost", {"fields": ("metal_cost", "crystal_cost", "deuterium_cost")}),
        )
    list_display = ("name", "target_level")

class ShipyardQueueAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("parent", "name", "count", "finished")}),
        ("Cost", {"fields": ("metal_cost", "crystal_cost", "deuterium_cost")}),
        )
    list_display = ("name", "count")

admin.site.register(models.UserProfile, UserAdmin)
admin.site.register(models.Planet, PlanetAdmin)
admin.site.register(models.Resources, ResourcesAdmin)
admin.site.register(models.Buildings, BuildingsAdmin)
admin.site.register(models.Technologies, TechnologyAdmin)
admin.site.register(models.Ships, ShipsAdmin)
admin.site.register(models.Defenses, DefensesAdmin)

admin.site.register(models.Queue, QueueAdmin)
admin.site.register(models.BuildingQueue, BuildingQueueAdmin)
admin.site.register(models.ResearchQueue, ResearchQueueAdmin)
admin.site.register(models.ShipyardQueue, ShipyardQueueAdmin)
