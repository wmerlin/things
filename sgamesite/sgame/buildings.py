# -*- coding: utf-8 -*-

import datetime
import math
from django.db import models
from sgame import core, queues
from sgame.core import ZeroField

"""All buildings (facilities are the same thing) in SGame."""

class Building(core.LeveledBase):
    """Represents a building.

    This object is usually bound to a planet."""

    # Most buildings use the same formula for calculating cost, which is:
    #     base_cost * E ** (level - 1)
    # base_cost depends on the particular building and must be set on each one.
    # E is usually 2, except on some resource buildings.
    cost_exponent_base = 2
    base_cost = None

    def level(self, now, when=None):
        return self.planet.buildings.level(self.name, now, when)

    def __init__(self, planet):
        if planet is not None:
            self.planet = planet

    def build_cost(self, level=None):
        """Calculate the cost of building the next level of this building.

        If level is not specified, it defaults to the one higher than the last
        level in the queue."""
        if level is None:
            level = self.future_level() + 1

        tmp = self.cost_exponent_base ** (level - 1)
        return (int(self.base_cost[0] * tmp), # Metal
                int(self.base_cost[1] * tmp), # Crystal
                int(self.base_cost[2] * tmp)) # Deuterium

    def _build_time(self, level, rf_level, nf_level):
        metal_cost, crystal_cost, deuterium_cost = self.build_cost(level)
        hours = float(metal_cost + crystal_cost) / (2500 * (rf_level + 1) * 2 ** nf_level)
        return datetime.timedelta(hours=hours)

    def build_time(self, level=None, when=None):
        """Calculate how long building a given level will take, based on cost
        and Robotics Factory/Nanite Factory levels."""
        if level is None:
            level = self.future_level() + 1

        # when is currently meant to be used from the building queue. If it's
        # specified we must check levels at when, but otherwise we want the
        # future level.
        rf_level = self.planet.buildings.level("robotics_factory", False, when)
        nf_level = self.planet.buildings.level("nanite_factory", False, when)
        return self._build_time(level, rf_level, nf_level)

    def have_enough_resources(self):
        metal_cost, crystal_cost, deuterium_cost = self.build_cost()
        return (metal_cost <= self.planet.resources.current_metal() and
                crystal_cost <= self.planet.resources.current_crystal() and
                deuterium_cost <= self.planet.resources.current_deuterium())

    def check_resources(self, queue_item):
        # Todo: Why are we recalculating the costs here?
        if not self.have_enough_resources():
            # Should send an in-game message here.
            print "Error: Cannot start", queue_item, "due to resource shortage."
            raise queues.CannotStart("Not enough resources")

    def is_in_use(self, now, when=None):
        """Determine whether this building is currently doing something. If it
        is, it can not be upgraded."""
        return False

    def check_not_in_use(self, queue_item):
        if self.is_in_use(False, queue_item.parent.start_time):
            # As above, should send a message
            print "Error: Cannot start", queue_item, "because building is in use."
            raise queues.CannotStart("Building in use.")

    def can_build(self):
        """Quick test to determine whether this can be queued."""
        if queues.BuildingQueue.objects.is_empty(self.planet):
            return (self.have_enough_resources() and
                    self.requirements_met(self.planet, True) and
                    not self.is_in_use(True))
        else:
            when = queues.BuildingQueue.objects.get_end_time(self.planet)
            return (self.requirements_met(self.planet, False, when) and
                    not self.is_in_use(False, when))

    def _finish(self, queue_item):
        queue_item.parent.planet.buildings.set_level(self.name, queue_item.target_level)
        queue_item.parent.planet.buildings.save()

class _ResourceBuilding(Building):
    def _finish(self, queue_item):
        # Make sure to change the level *after* we update resources. Production
        # rates have changed, yes, but not yet.
        queue_item.parent.planet.resources.update_resources(queue_item.parent.end_time)
        queue_item.parent.planet.resources.save()
        Building._finish(self, queue_item)

class ResourceBuilding(_ResourceBuilding):
    def production(self, level=None):
        if level is None:
            level = self.current_level()
        return self._production(level)

    def power_usage(self, level=None):
        if level is None:
            level = self.current_level()
        return self._power_usage(level)

class MetalMine(ResourceBuilding):
    name = "metal_mine"
    verbose_name = "Metal Mine"
    cost_exponent_base = 1.5
    base_cost = (60, 15, 0)

    def _production(self, level):
        return int(30 * level * 1.1 ** level)

    def _power_usage(self, level):
        return core.iroundup(10 * level * 1.1 ** level)

class CrystalMine(ResourceBuilding):
    name = "crystal_mine"
    verbose_name = "Crystal Mine"
    cost_exponent_base = 1.6
    base_cost = (48, 24, 0)

    def _production(self, level):
        return int(20 * level * 1.1 ** level)

    def _power_usage(self, level):
        return core.iroundup(10 * level * 1.1 ** level)

class DeuteriumSynthesizer(ResourceBuilding):
    name = "deuterium_synthesizer"
    verbose_name = "Deuterium Synthesizer"
    cost_exponent_base = 1.5
    base_cost = (225, 75, 0)

    def production(self, level=None):
        if level is None:
            level = self.current_level()
        mintemp = self.planet.minimum_temperature
        maxtemp = self.planet.maximum_temperature

        return int((10 * level * 1.1 ** level) *
                   (-0.002 * (mintemp + maxtemp) + 1.36))

    def _power_usage(self, level):
        return core.iroundup(20 * level * 1.1 ** level)

class SolarPlant(ResourceBuilding):
    name = "solar_plant"
    verbose_name = "Solar Plant"
    cost_exponent_base = 1.5
    base_cost = (75, 30, 0)

    def _production(self, level):
        return int(20 * level * 1.1 ** level)

class FusionReactor(ResourceBuilding):
    name = "fusion_reactor"
    verbose_name = "Fusion Reactor"
    cost_exponent_base = 1.8
    base_cost = (900, 360, 180)
    prerequisites = (
        ("deuterium_synthesizer", 5),
        ("energy_tech", 3),
        )

    def _production(self, level):
        energy_tech_level = self.planet.user.technologies.level("energy_tech", True)
        return int(30 * level * (1.05 + energy_tech_level * 0.01) ** level)

    def deuterium_usage(self, level=None):
        if level is None:
            level = self.current_level()
        return core.iroundup(10 * level * 1.1 ** level)

class Storage(_ResourceBuilding):
    def capacity(self, level=None):
        if level is None:
            level = self.current_level()
        return 5000 * int(2.5 * math.e ** (20 * float(level) / 33))

class MetalStorage(Storage):
    name = "metal_storage"
    verbose_name = "Metal Storage"
    base_cost = (1000, 0, 0)

class CrystalStorage(Storage):
    name = "crystal_storage"
    verbose_name = "Crystal Storage"
    base_cost = (1000, 500, 0)

class DeuteriumTank(Storage):
    name = "deuterium_tank"
    verbose_name = "Deuterium Tank"
    base_cost = (1000, 1000, 0)

# Robotics and Nanites would logically not build themselves, so we override
# build_time to zero those out. Note that I haven't tested this against OGame,
# but the times seem correct.

class RoboticsFactory(Building):
    name = "robotics_factory"
    verbose_name = "Robotics Factory"
    base_cost = (400, 120, 200)

    def build_time(self, level=None, when=None):
        nf_level = self.planet.buildings.level("nanite_factory", False, when)
        return self._build_time(level, 0, nf_level)

class Shipyard(Building):
    name = "shipyard"
    verbose_name = "Shipyard"
    base_cost = (400, 200, 100)
    prerequisites = (
       ("robotics_factory", 2),
       )

    def is_in_use(self, now, when=None):
        # If there's nothing in the building queue we simply check whether the
        # shipyard queue is empty. Otherwise we make sure it will be empty by
        # the time we get to build.
        if when is None:
            if now:
                return not queues.ShipyardQueue.objects.is_empty(self.planet)
            else:
                when = queues.BuildingQueue.objects.get_end_time(self.planet)
                if when is None:
                    return not queues.ShipyardQueue.objects.is_empty(self.planet)
        queue = queues.ShipyardQueue.objects.filter(parent__end_time__gt=when)
        return bool(queue.count())

class ResearchLab(Building):
    name = "research_lab"
    verbose_name = "Research Lab"
    base_cost = (200, 400, 200)

    def is_in_use(self, now, when=None):
        # If there's nothing in the building queue we simply check whether the
        # research queue is empty. Otherwise we make sure it will be empty by
        # the time we get to build.
        if when is None:
            if now:
                return not queues.ResearchQueue.objects.is_empty(self.planet)
            else:
                when = queues.BuildingQueue.objects.get_end_time(self.planet)
                if when is None:
                    return not queues.ResearchQueue.objects.is_empty(self.planet)
        queue = queues.ResearchQueue.objects.filter(parent__end_time__gt=when)
        return bool(queue.count())

class NaniteFactory(Building):
    name = "nanite_factory"
    verbose_name = "Nanite Factory"
    base_cost = (1000000, 500000, 100000)
    prerequisites = (
       ("robotics_factory", 10),
       ("computer_tech", 10),
       )

    def build_time(self, level=None, when=None):
        rf_level = self.planet.buildings.level("robotics_factory", False, when)
        return self._build_time(level, rf_level, 0)

# name -> Building
BuildingTypes = dict((klass.name, klass) for klass in [
    MetalMine, CrystalMine, DeuteriumSynthesizer,
    SolarPlant, FusionReactor,
    MetalStorage, CrystalStorage, DeuteriumTank,
    RoboticsFactory, Shipyard, ResearchLab, NaniteFactory
    ])

class Buildings(models.Model):
    class Meta(object):
        verbose_name_plural = "Buildings"

    owned_objects = BuildingTypes

    planet = models.OneToOneField(core.Planet, primary_key=True)

    # These are all the buildings OGame has. Most of these are currently
    # unimplemented, but this way I hopefully won't have to mess with the
    # database much in the future.
    #
    # Argh, so much repetition! We should be able to use Building.name here
    # to automatically generate these.
    metal_mine_level = ZeroField("Metal Mine")
    crystal_mine_level = ZeroField("Crystal Mine")
    deuterium_synthesizer_level = ZeroField("Deuterium Synthesizer")
    solar_plant_level = ZeroField("Solar Plant")
    fusion_reactor_level = ZeroField("Fusion Reactor")
    metal_storage_level = ZeroField("Metal Storage")
    crystal_storage_level = ZeroField("Crystal Storage")
    deuterium_tank_level = ZeroField("Deuterium Tank")

    robotics_factory_level = ZeroField("Robotics Factory")
    shipyard_level = ZeroField("Shipyard")
    research_lab_level = ZeroField("Research Lab")
    alliance_depot_level = ZeroField("Alliance Depot")
    missile_silo_level = ZeroField("Missile Silo")
    nanite_factory_level = ZeroField("Nanite Factory")
    terraformer_level = ZeroField("Terraformer")

    def __unicode__(self):
        return "Buildings on %s" % self.planet

    def get_class(self, name):
        return BuildingTypes[name]

    def get_object(self, name):
        return BuildingTypes[name](self.planet)

    def level(self, name, now, when=None):
        if when is None:
            if now:
                return getattr(self, name + "_level")
            else:
                last_item = queues.BuildingQueue.objects.get_last_item(self.planet, name)
                if last_item:
                    return last_item.target_level
                else:
                    return getattr(self, name + "_level")
        else:
            queue = (queues.BuildingQueue.objects.filter(
                parent__planet=self.planet, name=name, parent__end_time__lte=when).
                    order_by("-parent__end_time"))
            if queue:
                return queue[0].target_level
            else:
                return getattr(self, name + "_level")

    def set_level(self, name, level):
        setattr(self, name + "_level", level)
