# -*- coding: utf-8 -*-

import datetime
import math
import random
from django.db import models
import django.contrib.auth.models as auth_models

Future = datetime.datetime.max

def iroundup(i):
    return int(math.ceil(i))

def ZeroField(name=None):
    """An integer defaulting to zero."""
    return models.IntegerField(default=0, verbose_name=name)

class UserProfile(models.Model):
    # Currently this model is just a stub, but stuff like score and alliance
    # might go here later.

    # Can this be a OneToOneField?
    user = models.ForeignKey(auth_models.User, primary_key=True)

    # Convenience
    @property
    def username(self):
        return self.user.username

    def __unicode__(self):
        return self.user.__unicode__()

# This is called when a new auth_models.User object is created to set up their
# profile, home planet, etc. I should probably look into exactly what happens
# when the registration module creates a new user... I don't know, but it's
# probably not a good idea to be creating these objects for a user that might
# not even activate their account.
def create_new_user(sender, **kwargs):
    # Avoid circular imports
    from sgame import models

    if kwargs["created"]:
        user = UserProfile(user=kwargs["instance"])
        user.save()
        planet = Planet.objects.create_new_planet(user)
        planet.save()
        # Resources depend on buildings due to some stuff in __init__, so don't
        # reorder these without thinking first.
        buildings = models.Buildings(planet=planet)
        buildings.save()
        resources = models.Resources(planet=planet, last_update=datetime.datetime.now())
        resources.save()
        technologies = models.Technologies(user=user)
        technologies.save()
        ships = models.Ships(planet=planet)
        ships.save()
        defenses = models.Defenses(planet=planet)
        defenses.save()
models.signals.post_save.connect(create_new_user, sender=auth_models.User)

class PlanetManager(models.Manager):
    def create_new_planet(self, user):
        # We currently use a very simple algorithm: Choose a random system/slot
        # in galaxy 1 and increment it until an unused slot is found.
        galaxy, system, slot = 1, random.randint(1, 500), random.randint(1, 15)
        def exists():
            return self.filter(galaxy=galaxy, system=system, slot=slot).count()

        while exists():
            slot += 1
            if slot > 15:
                slot = 1
                system += 1
            if system > 500:
                system = 1
                galaxy += 1
            if galaxy > 9:
                # Panic
                raise Exception("Can't find an empty slot for the new planet.")

        # We should choose some temperatures here, based on its slot. And when
        # we get shiny graphics, pick one of those too...
        return Planet(user=user, name="Home planet",
            galaxy=galaxy, system=system, slot=slot)

class Planet(models.Model):
    """Information about a planet."""
    objects = PlanetManager()

    user = models.ForeignKey(UserProfile)
    name = models.CharField(max_length=30)

    # These shouldn't have defaults...
    minimum_temperature = models.IntegerField(default=100)
    maximum_temperature = models.IntegerField(default=-40)

    galaxy = models.IntegerField()
    system = models.IntegerField()
    slot = models.IntegerField()

    def __unicode__(self):
        return "%s %s" % (self.name, self.location())

    def location(self):
        """Returns a formatted location."""
        return "[%s:%s:%s]" % (self.galaxy, self.system, self.slot)

    def get_class(self, name):
        for model in [self.buildings, self.user.technologies, self.ships, self.defenses]:
            try:
                return model.get_class(name)
            except KeyError:
                pass

        raise ValueError("Unknown object")

    def get_object(self, name):
        for model in [self.buildings, self.user.technologies, self.ships, self.defenses]:
            try:
                return model.get_object(name)
            except KeyError:
                pass

        raise ValueError("Unknown object")

    def level(self, name, now, when=None):
        for model in [self.buildings, self.user.technologies, self.ships, self.defenses]:
            if name in model.owned_objects:
                return model.level(name, now, when)
        raise ValueError("Unknown object")

class ObjectBase(object):
    """Base class for everything. Buildings, technologies, ships, defense. If
    they share common methods, they'll be found here."""

    # Short name for this object, e.g. "metal_mine". This is the internal name
    # and will be frequently hardcoded.
    name = None
    # Display name for this building, e.g. "Metal Mine". This is used in
    # templates.
    verbose_name = None

    # A list of (name, level) prerequisites.
    prerequisites = ()

    def check_resources(self, queue_item):
        pass

    def requirements_met(self, planet, now, when=None):
        """Determine if ship prerequisites are met. This includes shipyard
        level and technologies."""
        for (name, level) in self.prerequisites:
            if planet.level(name, now, when) < level:
                return False
        return True

    def check_requirements(self, queue_item):
        if not self.requirements_met(
            queue_item.parent.planet, False, queue_item.parent.start_time):
            # As above, should send a message
            print "Error: Cannot start", queue_item, "due to missing requirements."
            raise queues.CannotStart("Requirements not met.")

    def check_not_in_use(self, queue_item):
        pass

    def check_parent_available(self, queue_item):
        pass

    def pre_start_check(self, queue_item):
        self.check_resources(queue_item)
        self.check_requirements(queue_item)
        self.check_not_in_use(queue_item)
        self.check_parent_available(queue_item)

    def start(self, queue_item):
        """Begin construction of these ships/defenses."""
        self.pre_start_check(queue_item)

        resources = queue_item.parent.planet.resources
        resources.update_resources(queue_item.parent.start_time)
        resources.metal -= queue_item.metal_cost
        resources.crystal -= queue_item.crystal_cost
        resources.deuterium -= queue_item.deuterium_cost
        resources.save()
        queue_item.parent.started = True
        queue_item.parent.save()

    def finish(self, queue_item):
        if not queue_item.parent.started:
            # This might throw an exception, but it will be caught at a higher
            # level.
            self.start(queue_item)
        self._finish(queue_item)

class LeveledBase(ObjectBase):
    def current_level(self):
        return self.level(True)
    def future_level(self):
        return self.level(False)
