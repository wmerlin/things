# -*- coding: utf-8 -*-

from django.db import models
from sgame import core, shipyard
from sgame.core import ZeroField

"""All defenses in SGame."""

class Defense(shipyard.ShipyardObject):
    """Represents one type of defense.

    This object is usually bound to a planet."""

    @property
    def count(self):
        return self.planet.defenses.count(self.name)

    def _touch(self, queue_item, new_items, now):
        current_items = queue_item.parent.planet.defenses.count(queue_item.name)
        queue_item.parent.planet.defenses.set_count(queue_item.name, current_items + new_items)
        queue_item.parent.planet.defenses.save()

    def _finish(self, queue_item):
        current_items = queue_item.parent.planet.defenses.count(queue_item.name)
        new_items = queue_item.count - queue_item.finished
        queue_item.parent.planet.defenses.set_count(queue_item.name, current_items + new_items)
        queue_item.parent.planet.defenses.save()

class RocketLauncher(Defense):
    name = "rocket_launcher"
    verbose_name = "Rocket Launcher"
    base_cost = (2000, 0, 0)
    prerequisites = (
        ("shipyard", 1),
        )

class LightLaser(Defense):
    name = "light_laser"
    verbose_name = "Light Laser"
    base_cost = (1500, 500, 0)
    prerequisites = (
        ("shipyard", 2),
        ("energy_tech", 1),
        ("laser_tech", 3),
        )

class HeavyLaser(Defense):
    name = "heavy_laser"
    verbose_name = "Heavy Laser"
    base_cost = (6000, 2000, 0)
    prerequisites = (
        ("shipyard", 4),
        ("energy_tech", 3),
        ("laser_tech", 6),
        )

class IonCannon(Defense):
    name = "ion_cannon"
    verbose_name = "Ion Cannon"
    base_cost = (2000, 6000, 0)
    prerequisites = (
        ("shipyard", 4),
        ("ion_tech", 4),
        )

class GaussCannon(Defense):
    name = "gauss_cannon"
    verbose_name = "Gauss Cannon"
    base_cost = (20000, 15000, 2000)
    prerequisites = (
        ("shipyard", 6),
        ("energy_tech", 6),
        ("weapons_tech", 3),
        ("shielding_tech", 1),
        )

class PlasmaTurret(Defense):
    name = "plasma_turret"
    verbose_name = "Plasma Turret"
    base_cost = (50000, 50000, 30000)
    prerequisites = (
        ("shipyard", 8),
        ("plasma_tech", 7),
        )

class SmallShieldDome(Defense):
    name = "small_shield_dome"
    verbose_name = "Small Shield Dome"
    base_cost = (10000, 10000, 0)
    prerequisites = (
        ("shipyard", 1),
        ("shielding_tech", 2),
        )

class LargeShieldDome(Defense):
    name = "large_shield_dome"
    verbose_name = "Large Shield Dome"
    base_cost = (50000, 50000, 0)
    prerequisites = (
        ("shipyard", 6),
        ("shielding_tech", 6),
        )

# name -> Defense
DefenseTypes = dict((klass.name, klass) for klass in [
    RocketLauncher, LightLaser, HeavyLaser, IonCannon, GaussCannon,
    PlasmaTurret, SmallShieldDome, LargeShieldDome,
    ])

class Defenses(models.Model):
    """Ship counts."""
    class Meta(object):
        verbose_name_plural = "Defenses"

    owned_objects = DefenseTypes

    planet = models.OneToOneField(core.Planet, primary_key=True)

    rocket_launcher_count = ZeroField()
    light_laser_count = ZeroField()
    heavy_laser_count = ZeroField()
    ion_cannon_count = ZeroField()
    gauss_cannon_count = ZeroField()
    plasma_turret_count = ZeroField()
    # These could also be thought of as boolean, but I think it's better this
    # way.
    small_shield_dome_count = ZeroField()
    large_shield_dome_count = ZeroField()

    def __unicode__(self):
        return "Defenses on %s" % self.planet

    def get_class(self, name):
        return DefenseTypes[name]

    def get_object(self, name):
        return DefenseTypes[name](self.planet)

    def count(self, name):
        return getattr(self, name + "_count")

    def set_count(self, name, count):
        setattr(self, name + "_count", count)
