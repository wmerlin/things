# -*- coding: utf-8 -*-

import datetime
from django.db import models
from sgame import core

# The queue code makes use of several tables. There is one generic, master
# queue and several more specific ones. The master table includes timestamps
# (making it easy to query for stuff that's done/partially done) and certain
# common information, and the subtables contain anything else.

class CannotStart(Exception):
    """Thrown when a queue item cannot be started, for whatever reason."""

class QueueManager(models.Manager):
    def process_queue(self, user, time=None):
        if time is None:
            time = datetime.datetime.now()

        def get_subitem(parent):
            subqueue = QueueTypes[parent.subqueue_name]
            subitem = subqueue.objects.get(parent=parent)
            return subitem

        queue = self.filter(user=user, end_time__lt=time).order_by("end_time")
        for item in queue:
            subitem = get_subitem(item)
            try:
                # Todo: We should think about calling start() by ourselves here
                # if it hasn't already. It might simplify things, particularly
                # finish().
                subitem.finish()
            except CannotStart as error:
                item.delete()
                self.fix_queue(item)
        queue.delete()

        running_items = self.filter(user=user, start_time__lt=time)
        for item in running_items:
            subitem = get_subitem(item)
            if not item.started:
                try:
                    subitem.start()
                except CannotStart as error:
                    item.delete()
                    self.fix_queue(item)
            else:
                subitem.touch()

    def fix_queue(self, source_item):
        # Purpose: To shuffle the queue around when one item has to go.
        # This entails:
        # - Checking that prerequisites are still met
        # - Fixing timestamps
        # It's quite possible (and even likely) that more objects will delete
        # themselves. In this case we can simply keep on going.
        #
        # Now, since we have more than one queue, we have to keep more than one
        # "current time" or we'll really mess up the whole thing.

        times = {}
        affected_items = Queue.objects.filter(
            user=source_item.user, start_time__gte=source_item.end_time)
        for item in affected_items:
            if item.subqueue_name not in times:
                if name == source_item.subqueue_name:
                    # Ahah! The offender! It's going to disappear now, so we
                    # special case this to move back the starting time.
                    times[item.subqueue_name] = source_item.start_time
                else:
                    times[item.subqueue_name] = item.start_time
            times[item.subqueue_name] = item.fixup(times[item.subqueue_name])

class Queue(models.Model):
    objects = QueueManager()

    user = models.ForeignKey(core.UserProfile)
    planet = models.ForeignKey(core.Planet)

    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    # A queue item is always in one of three states:
    # - waiting
    # - in progress
    # - finished
    # The timestamps alone are enough to figure this out, but the transition
    # from the first phase to the second phase involves deducting resources.
    # Naturally we'd rather that didn't happen more than once, hence this
    # field...
    started = models.BooleanField(default=False)

    # There should to be a way to reference another table, but this'll do.
    subqueue_name = models.CharField(max_length=30)

    def __unicode__(self):
        return "Queued %s for %s" % (self.subqueue_name, self.user)

class SubqueueManager(models.Manager):
    def get_last_item(self, planet, name, default=None):
        """Returns the last item in the queue for the given planet."""
        if planet is None:
            # Cheap way to not filter by planet, since the research queue isn't
            # exactly tied to planets. Except they are.
            queue = self.filter(name=name).order_by("-parent__end_time")
        else:
            queue = self.filter(parent__planet=planet, name=name).order_by("-parent__end_time")

        if queue:
            return queue[0]
        else:
            return default

    def get_end_time(self, planet, default=None):
        """Returns the time at which the queue ends, or something else (default
        None)."""
        if planet is None:
            queue = self.order_by("-parent__end_time")
        else:
            queue = self.filter(parent__planet=planet).order_by("-parent__end_time")

        if queue:
            return queue[0].parent.end_time
        else:
            return default

    def is_running_at(self, planet, name, start_time, end_time):
        query = self.filter(name=name)
        if planet is not None:
            query = query.filter(parent__planet=planet)
        if start_time is not None:
            query = query.filter(parent__start_time__lt=end_time)
        if end_time is not None:
            query = query.filter(parent__end_time__gt=start_time)

        return bool(query.count())

    def is_empty(self, planet):
        if planet is None:
            return not bool(self.count())
        else:
            return not self.filter(parent__planet=planet).count()

class Subqueue(models.Model):
    class Meta:
        abstract = True

    objects = SubqueueManager()

    # This reference goes both ways, but going in the other direction is almost
    # useless, because there's one reference for each possible subqueue.
    parent = models.OneToOneField(Queue)

    metal_cost = models.IntegerField(default=0)
    crystal_cost = models.IntegerField(default=0)
    deuterium_cost = models.IntegerField(default=0)

    name = models.CharField(max_length=30)

    def __unicode__(self):
        return "Queued %s on %s" % (self.name, self.parent.planet)

    def start(self):
        # This can throw an exception, but we can't do anything about it here.
        self.item.start(self)

    def touch(self):
        # Only used in ShipardQueue
        pass

    def finish(self):
        self.item.finish(self)

class BuildingQueue(Subqueue):
    target_level = models.IntegerField()

    # This isn't currently used anywhere, and we should be careful before
    # enabling it. It could easily mess up the ability to rearrange the queue.
    deconstructing = models.BooleanField(default=False)

    @property
    def item(self):
        return self.parent.planet.buildings.get_object(self.name)

    def fixup(self, time):
        try:
            # Check requirements. *Don't* check resources.
            self.item.check_requirements(self)
            self.item.check_not_in_use(self)
        except CannotStart as error:
            self.parent.delete()
            return time
        else:
            # Fix build time, and return the new one.
            build_time = self.building.build_time(target_level, when=time)
            self.start_time = time
            self.end_time = time + build_time
            return self.end_time

class ResearchQueue(Subqueue):
    target_level = models.IntegerField()

    @property
    def item(self):
        return self.parent.user.technologies.get_technology(self.name)

    def fixup(self, time):
        try:
            # Check requirements. *Don't* check resources.
            self.item.check_requirements(self)
        except CannotStart as error:
            self.parent.delete()
            return time
        else:
            # Fix research time, and return the new one.
            research_time = self.item.research_time(
                self.parent.planet, self.target_level, when=time)
            self.start_time = time
            self.end_time = time + research_time
            return self.end_time

class ShipyardQueue(Subqueue):
    # Note that this queue is for both ships and defense, even though they are
    # stored in different tables.

    # The full ship count
    count = models.IntegerField()
    # The number finished so far
    finished = models.IntegerField(default=0)

    @property
    def item(self):
        return self.parent.planet.get_object(self.name)

    def __unicode__(self):
        return "%s queued %s on %s" % (self.count, self.name, self.parent.planet)

    def touch(self):
        self.item.touch(self)

    def fixup(self, time):
        try:
            self.item.check_requirements(self)
        except CannotStart as error:
            self.parent.delete()
            return time
        else:
            # Fix build time, and return the new one.
            build_time = self.item.build_time(self.count, when=time)
            self.start_time = time
            self.end_time = time + build_time
            return self.end_time

# Maps the various values of Queue.subqueue_name to models.
QueueTypes = {
    "building": BuildingQueue,
    "research": ResearchQueue,
    "shipyard": ShipyardQueue,
    }
