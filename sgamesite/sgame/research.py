# -*- coding: utf-8 -*-

import datetime
from django.db import models
from sgame import core, queues
from sgame.core import ZeroField

"""All technologies in SGame."""

class Technology(core.LeveledBase):
    """Represents a technology.

    This object is usually bound to a user."""

    def level(self, now, when=None):
        return self.user.technologies.level(self.name, now, when)

    # A tuple of (Technology, level) prerequisites.
    prerequisites = ()

    # This is almost always 2, except in Astrophysics
    cost_exponent_base = 2

    def __init__(self, planet):
        if planet is not None:
            self.planet = planet

    def research_cost(self, level=None):
        """Calculate the cost of researching a given level of this technology.

        Defaults to the last level in the queue + 1."""
        if level is None:
            level = self.future_level() + 1

        tmp = self.cost_exponent_base ** (level - 1)
        return (int(self.base_cost[0] * tmp), # Metal
                int(self.base_cost[1] * tmp), # Crystal
                int(self.base_cost[2] * tmp)) # Deuterium

    def research_time(self, level=None, when=None):
        """Calculate how long researching a given level will take, based on
        cost and Research Lab level."""
        if level is None:
            level = self.future_level() + 1

        rl_level = self.planet.buildings.level("research_lab", False, when)
        metal_cost, crystal_cost, deuterium_cost = self.research_cost(level)
        hours = float(metal_cost + crystal_cost) / (1000 * (1 + rl_level))
        return datetime.timedelta(hours=hours)

    def have_enough_resources(self):
        metal_cost, crystal_cost, deuterium_cost = self.research_cost()
        return (metal_cost <= self.planet.resources.current_metal() and
                crystal_cost <= self.planet.resources.current_crystal() and
                deuterium_cost <= self.planet.resources.current_deuterium())

    def check_resources(self, queue_item):
        # Todo: Why are we recalculating the costs here?
        if not self.have_enough_resources():
            # Should send an in-game message here.
            print "Error: Cannot start", queue_item, "due to resource shortage."
            raise queues.CannotStart("Not enough resources")

    def parent_available(self, now, when=None, level=None):
        if level is None:
            level = self.future_level() + 1
        if when is None:
            if now:
                when = datetime.datetime.now()
            else:
                when = queues.ResearchQueue.objects.get_end_time(None)
                if when is None:
                    when = datetime.datetime.now()
        end_time = when + self.research_time(level)
        return not queues.BuildingQueue.objects.is_running_at(
            self.planet, "research_lab", when, end_time)

    def check_parent_available(self, queue_item):
        if not self.parent_available(True, level=queue_item.target_level):
            print "Error: Cannot start", queue_item, "due to unavailable parent."
            raise queues.CannotStart("Parent building unavailable.")

    def can_research(self):
        """Quick test to determine whether this can be queued."""
        if queues.ResearchQueue.objects.is_empty(None):
            return (self.have_enough_resources() and
                    self.requirements_met(True) and
                    self.parent_available(True))
        else:
            when = queues.ResearchQueue.objects.get_end_time(None)
            return (self.requirements_met(False, when) and
                    self.parent_available(False, when))

    def _finish(self, queue_item):
        queue_item.parent.user.technologies.set_level(self.name, queue_item.target_level)
        queue_item.parent.user.technologies.save()

class EnergyTechnology(Technology):
    name = "energy_tech"
    verbose_name = "Energy Technology"
    base_cost = (0, 800, 400)
    prerequisites = (
        ("research_lab", 1),
        )

class LaserTechnology(Technology):
    name = "laser_tech"
    verbose_name = "Laser Technology"
    base_cost = (200, 100, 0)
    prerequisites = (
        ("research_lab", 1),
        ("energy_tech", 2),
        )

class IonTechnology(Technology):
    name = "ion_tech"
    verbose_name = "Ion Technology"
    base_cost = (1000, 300, 100)
    prerequisites = (
        ("research_lab", 4),
        ("energy_tech", 4),
        ("laser_tech", 5),
        )

class HyperspaceTechnology(Technology):
    name = "hyperspace_tech"
    verbose_name = "Hyperspace Technology"
    base_cost = (0, 4000, 2000)
    prerequisites = (
        ("research_lab", 7),
        ("energy_tech", 5),
        ("shielding_tech", 5),
        )

class PlasmaTechnology(Technology):
    name = "plasma_tech"
    verbose_name = "Plasma Technology"
    base_cost = (2000, 4000, 100)
    prerequisites = (
        ("research_lab", 4),
        ("energy_tech", 8),
        ("laser_tech", 10),
        ("ion_tech", 5),
        )

class CombustionDrive(Technology):
    name = "combustion_drive"
    verbose_name = "Combustion Drive"
    base_cost = (400, 0, 600)
    prerequisites = (
        ("research_lab", 1),
        ("energy_tech", 1),
        )

class ImpulseDrive(Technology):
    name = "impulse_drive"
    verbose_name = "Impulse Drive"
    base_cost = (2000, 4000, 600)
    prerequisites = (
        ("research_lab", 2),
        ("energy_tech", 1),
        )

class HyperspaceDrive(Technology):
    name = "hyperspace_drive"
    verbose_name = "Hyperspace Drive"
    base_cost = (10000, 20000, 6000)
    prerequisites = (
        ("research_lab", 7),
        ("hyperspace_tech", 3),
        )

class EspionageTechnology(Technology):
    name = "espionage_tech"
    verbose_name = "Espionage Technology"
    base_cost = (200, 1000, 200)
    prerequisites = (
        ("research_lab", 3),
        )

class ComputerTechnology(Technology):
    name = "computer_tech"
    verbose_name = "Computer Technology"
    base_cost = (0, 400, 600)
    prerequisites = (
        ("research_lab", 1),
        )

class Astrophysics(Technology):
    name = "astrophysics"
    verbose_name = "Astrophysics"
    cost_exponent_base = 1.75
    base_cost = (4000, 8000, 4000)
    prerequisites = (
        ("research_lab", 3),
        ("espionage_tech", 4),
        ("impulse_drive", 3),
        )

class IntergalacticResearchNetwork(Technology):
    name = "intergalactic_research_net"
    verbose_name = "Ingergalactic Research Network"
    base_cost = (240000, 400000, 160000)
    prerequisites = (
        ("research_lab", 10),
        ("computer_tech", 8),
        ("hyperspace_tech", 8),
        )

class GravitonTechnology(Technology):
    name = "graviton_tech"
    verbose_name = "Graviton Technology"
    base_cost = (0, 0, 0)
    energy_cost = 300000
    prerequisites = (
        ("research_lab", 12),
        )

    def have_enough_resources(self):
        # The energy costs go up with each level, but we don't reflect that
        # here, because I'm not sure how it goes up. Oh well, it doesn't matter
        # much anyway.
        available_energy = self.planet.resources.current_power()
        return available_energy >= self.energy_cost

class ArmourTechnology(Technology):
    name = "armour_tech"
    verbose_name = "Armour Technology"
    base_cost = (1000, 0, 0)
    prerequisites = (
        ("research_lab", 2),
        )

class WeaponsTechnology(Technology):
    name = "weapons_tech"
    verbose_name = "Weapons Technology"
    base_cost = (800, 200, 0)
    prerequisites = (
        ("research_lab", 4),
        )

class ShieldingTechnology(Technology):
    name = "shielding_tech"
    verbose_name = "Shielding Technology"
    base_cost = (200, 600, 0)
    prerequisites = (
        ("research_lab", 6),
        )

# name -> Technology
TechnologyTypes = dict((klass.name, klass) for klass in [
    EnergyTechnology, LaserTechnology, IonTechnology, HyperspaceTechnology,
    PlasmaTechnology,

    CombustionDrive, ImpulseDrive, HyperspaceDrive,

    EspionageTechnology, ComputerTechnology, Astrophysics,
    IntergalacticResearchNetwork, GravitonTechnology,

    ArmourTechnology, WeaponsTechnology, ShieldingTechnology,
    ])

class Technologies(models.Model):
    """Technology levels."""
    class Meta(object):
        verbose_name_plural = "Technologies"

    owned_objects = TechnologyTypes

    user = models.OneToOneField(core.UserProfile, primary_key=True)

    # All the technologies OGame has. I don't even have plans for a research
    # lab yet...
    energy_tech_level = ZeroField("Energy Tech")
    laser_tech_level = ZeroField("Laser Tech")
    ion_tech_level = ZeroField("Ion Tech")
    hyperspace_tech_level = ZeroField("Hyperspace Tech")
    plasma_tech_level = ZeroField("Plasma Tech")

    combustion_drive_level = ZeroField("Combustion Drive")
    impulse_drive_level = ZeroField("Impulse Drive")
    hyperspace_drive_level = ZeroField("Hyperspace Drive")

    espionage_tech_level = ZeroField("Espionage Tech")
    computer_tech_level = ZeroField("Computer Tech")
    astrophysics_level = ZeroField("Astrophysics")
    intergalactic_research_net_level = ZeroField("Intergalactic Research Network")
    graviton_tech_level = ZeroField("Graviton Tech")

    armour_tech_level = ZeroField("Armour Tech")
    weapons_tech_level = ZeroField("Weapons Tech")
    shielding_tech_level = ZeroField("Shielding Tech")

    # A lot of these methods are very similar to Buildings... I wonder what the
    # base class should be called.

    def __init__(self, *args, **kwargs):
        models.Model.__init__(self, *args, **kwargs)

        try:
            user = self.user
        except core.UserProfile.DoesNotExist:
            pass
        else:
            for name, klass in TechnologyTypes.items():
                setattr(self, name, klass(self.user))

    def __unicode__(self):
        return "Technologies for %s" % self.user

    def get_class(self, name):
        return TechnologyTypes[name]

    def get_object(self, name, planet):
        return TechnologyTypes[name](planet)

    def level(self, name, now, when=None):
        if when is None:
            if now:
                return getattr(self, name + "_level")
            else:
                last_item = queues.ResearchQueue.objects.get_last_item(None, name)
                if last_item:
                    return last_item.target_level
                else:
                    return getattr(self, name + "_level")
        else:
            queue = (queues.ResearchQueue.objects.filter(
                name=name, parent__end_time__lte=when).order_by("-parent__end_time"))
            if queue:
                return queue[0].target_level
            else:
                return getattr(self, name + "_level")

    def set_level(self, name, level):
        setattr(self, name + "_level", level)
