# -*- coding: utf-8 -*-

import datetime
from django.db import models
from sgame import core

class Resources(models.Model):
    """Resource information on a planet.

    Since resources are stored as a base value and an offset from the last
    modification time, make sure to update this when appropriate, or your
    values will be drasically wrong."""
    class Meta(object):
        verbose_name_plural = "Resources"

    planet = models.OneToOneField(core.Planet, primary_key=True)

    # These are the resources you begin with.
    metal = models.FloatField(default=500.0)
    crystal = models.FloatField(default=500.0)
    deuterium = models.FloatField(default=0.0)

    last_update = models.DateTimeField()

    # Power settings
    metal_mine_power_factor = models.FloatField(default=1.0)
    crystal_mine_power_factor = models.FloatField(default=1.0)
    deuterium_synthesizer_power_factor = models.FloatField(default=1.0)
    solar_plant_power_factor = models.FloatField(default=1.0)
    fusion_reactor_power_factor = models.FloatField(default=1.0)

    def __init__(self, *args, **kwargs):
        models.Model.__init__(self, *args, **kwargs)
        try:
            self.planet
        except core.Planet.DoesNotExist:
            # Bah
            return

        self.rebalance()

    def rebalance(self):
        # This function's purpose is twofold:
        # 1) To calculate lots of handy little variables that are otherwise
        #    basically just copy&paste.
        # 2) To distribute power fairly in the event of a shortage.
        # cache_mine() does most of the first, with lots of setattr() calls.
        #
        # The rest is computed by approximation. It's not hard to figure out
        # power in ideal conditions (there's nothing to do), so obviously most
        # of this function is set up to deal with the other case.
        #
        # The relationship between power and production can be expressed as
        # follows:
        #    mx + cx + dx = s + min(rz, dxyz)
        # Where m, c, and d are the "ideal power draws" of each mine; r is the
        # amount of deuterium the reactor uses, y is the amount of deuterium
        # produced for each unit of power, and z is the amount of fusion power
        # generated for each unit of deuterium. x is a scaling factor to reduce
        # power consumption to available levels, and is what this function
        # computes.
        #
        # The difficulty in this is isolating x in the above equation. This is
        # beyond my mathematical capabilites and as such we use a fairly simple
        # approximation algorithm. We use 1 as the initial value of x and
        # solve, then use the resulting value as input for the next iteration.
        # This is repeated twenty times and the last value is kept as output.

        def cache_mine(shortname, name, draws, reduction_factor):
            def _set(attr, to):
                setattr(self, shortname + "_" + attr, to)
            building = self.planet.get_object(name)
            power_factor = getattr(self, name + "_power_factor")

            _set("peak_production", building.production())
            _set("ideal_production", building.production() * power_factor)
            if draws:
                _set("peak_draw", building.power_usage())
                _set("ideal_draw", building.power_usage() * power_factor)
            _set("actual_production", building.production() * power_factor * reduction_factor)
            if draws:
                _set("actual_draw", building.power_usage() * power_factor * reduction_factor)

        def cache_all(reduction_factor):
            cache_mine("mm", "metal_mine", True, reduction_factor)
            cache_mine("cm", "crystal_mine", True, reduction_factor)
            cache_mine("ds", "deuterium_synthesizer", True, reduction_factor)
            cache_mine("sp", "solar_plant", False, reduction_factor)
            cache_mine("fr", "fusion_reactor", False, reduction_factor)
        cache_all(1)

        def solve(m, c, d, s, r, y, z, x):
            fr_available = min(r, d * x * y)
            power = s + min(r * z, d * x * y * z)
            draw = (m + c + d)
            if draw:
                result = power / draw
            else:
                result = 1
            return fr_available, result

        deut_per_power = (float(self.ds_peak_production) / self.ds_peak_draw) if self.ds_peak_draw else 0.0
        fr_usage = self.planet.get_object("fusion_reactor").deuterium_usage()
        fusion_power_per_deut = (float(self.fr_peak_production) / fr_usage) if fr_usage else 0.0

        reduction_factor = 1
        for i in range(20):
            fr_available, reduction_factor = solve(
                self.mm_ideal_draw, self.cm_ideal_draw, self.ds_ideal_draw,
                self.sp_ideal_production, fr_usage, deut_per_power,
                fusion_power_per_deut, reduction_factor)
            reduction_factor = min(reduction_factor, 1)

        cache_all(reduction_factor)
        self.fr_actual_production = fusion_power_per_deut * fr_available
        self.fr_deuterium_usage = fr_available

    def __repr__(self):
        return "Resources on %s" % self.planet

    # Production

    def metal_production(self):
        return self.mm_actual_production + 20
    def crystal_production(self):
        return self.cm_actual_production + 10
    def deuterium_production(self):
        return self.ds_actual_production - self.fr_deuterium_usage

    def generated_power(self):
        ss = self.planet.ships.get_object("solar_satellite")
        return (self.sp_actual_production + self.fr_actual_production +
                (ss.count() * ss.production()))
    def ideal_power_draw(self):
        return self.mm_actual_draw + self.cm_actual_draw + self.ds_actual_draw
    def current_power(self):
        return self.generated_power() - self.ideal_power_draw()

    # Update code.

    def _seconds_since_last_update(self, time):
        """Calculate the number of seconds since the last update was performed."""
        delta = time - self.last_update
        seconds = (delta.seconds + (float(delta.microseconds) / 1000000) +
                   (delta.days * 24 * 60 * 60))
        return seconds

    def _current_resources_for(self, start, per_hour_func, seconds):
        """Returns the current amount of a given type of resources.

        @param start: The original amount of resources.
        @param per_hour: How much of this is generated every hour.
        @param seconds: The number of seconds elapsed.
        """
        per_second = float(per_hour_func()) / 60 / 60
        return start + (seconds * per_second)

    def update_resources(self, time):
        """Update the amount of resources, based on current production rates."""
        seconds = self._seconds_since_last_update(time)

        # Power generation may have shifted
        self.rebalance()

        self.metal = self._current_resources_for(
            self.metal, self.metal_production, seconds)
        self.crystal = self._current_resources_for(
            self.crystal, self.crystal_production, seconds)
        self.deuterium = self._current_resources_for(
            self.deuterium, self.deuterium_production, seconds)

        self.last_update = time

    def current_metal(self):
        produced = int(self._current_resources_for(self.metal, self.metal_production,
                       self._seconds_since_last_update(datetime.datetime.now())))
        cap = self.planet.get_object("metal_storage").capacity()
        return min(produced, cap)

    def current_crystal(self):
        produced = int(self._current_resources_for(self.crystal, self.crystal_production,
                       self._seconds_since_last_update(datetime.datetime.now())))
        cap = self.planet.get_object("crystal_storage").capacity()
        return min(produced, cap)

    def current_deuterium(self):
        produced = int(self._current_resources_for(self.deuterium, self.deuterium_production,
                       self._seconds_since_last_update(datetime.datetime.now())))
        cap = self.planet.get_object("deuterium_tank").capacity()
        return min(produced, cap)
