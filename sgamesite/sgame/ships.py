# -*- coding: utf-8 -*-

from django.db import models
from sgame import core, shipyard
from sgame.core import ZeroField

"""All ships in SGame."""

class Ship(shipyard.ShipyardObject):
    """Represents one class of ship.

    This object is usually bound to a planet."""

    def count(self):
        return self.planet.ships.count(self.name)

    def _touch(self, queue_item, new_items, now):
        current_items = self.planet.ships.count(queue_item.name)
        self.planet.ships.set_count(queue_item.name, current_items + new_items)
        self.planet.ships.save()

    def _finish(self, queue_item):
        ships = queue_item.parent.planet.ships
        current_items = ships.count(queue_item.name)
        new_items = queue_item.count - queue_item.finished
        ships.set_count(queue_item.name, current_items + new_items)
        ships.save()

class LightFighter(Ship):
    name = "light_fighter"
    verbose_name = "Light Fighter"
    base_cost = (3000, 1000, 0)
    prerequisites = (
        ("shipyard", 2),
        ("combustion_drive", 1),
        )

class HeavyFighter(Ship):
    name = "heavy_fighter"
    verbose_name = "Heavy Fighter"
    base_cost = (6000, 4000, 0)
    prerequisites = (
        ("shipyard", 3),
        ("impulse_drive", 2),
        ("armour_tech", 2),
        )

class Cruiser(Ship):
    name = "cruiser"
    verbose_name = "Cruiser"
    base_cost = (20000, 7000, 2000)
    prerequisites = (
        ("shipyard", 5),
        ("impulse_drive", 4),
        ("ion_tech", 2),
        )

class Bomber(Ship):
    name = "bomber"
    verbose_name = "Bomber"
    base_cost = (50000, 25000, 15000)
    prerequisites = (
        ("shipyard", 8),
        ("impulse_drive", 6),
        ("plasma_tech", 5),
        )

class Battleship(Ship):
    name = "battleship"
    verbose_name = "Battleship"
    base_cost = (45000, 15000, 0)
    prerequisites = (
        ("shipyard", 7),
        ("hyperspace_drive", 2),
        )

class Battlecruiser(Ship):
    name = "battlecruiser"
    verbose_name = "Battlecruiser"
    base_cost = (30000, 40000, 15000)
    prerequisites = (
        ("shipyard", 8),
        ("hyperspace_drive", 5),
        ("hyperspace_tech", 5),
        ("laser_tech", 12),
        )

class Destroyer(Ship):
    name = "destroyer"
    verbose_name = "Destroyer"
    base_cost = (60000, 50000, 15000)
    prerequisites = (
        ("shipyard", 9),
        ("hyperspace_drive", 6),
        ("hyperspace_tech", 5),
        )

class DeathStar(Ship):
    name = "death_star"
    verbose_name = "Death Star"
    base_cost = (5000000, 4000000, 1000000)
    prerequisites = (
        ("shipyard", 12),
        ("graviton_tech", 1),
        )

class SmallCargoShip(Ship):
    name = "small_cargo_ship"
    verbose_name = "Small Cargo Ship"
    base_cost = (2000, 2000, 0)
    prerequisites = (
        ("shipyard", 2),
        ("combustion_drive", 2),
        )

class LargeCargoShip(Ship):
    name = "large_cargo_ship"
    verbose_name = "Large Cargo Ship"
    base_cost = (6000, 6000, 0)
    prerequisites = (
        ("shipyard", 4),
        ("combustion_drive", 6),
        )

class Recycler(Ship):
    name = "recycler"
    verbose_name = "Recycler"
    base_cost = (10000, 6000, 2000)
    prerequisites = (
        ("shipyard", 4),
        ("combustion_drive", 6),
        ("shielding_tech", 2),
        )

class ColonyShip(Ship):
    name = "colony_ship"
    verbose_name = "Colony Ship"
    base_cost = (10000, 20000, 10000)
    prerequisites = (
        ("shipyard", 4),
        ("impulse_drive", 3),
        )

class EspionageProbe(Ship):
    name = "espionage_probe"
    verbose_name = "Espionage Probe"
    base_cost = (0, 1000, 0)
    prerequisites = (
        ("shipyard", 3),
        ("combustion_drive", 3),
        )

class SolarSatellite(Ship):
    name = "solar_satellite"
    verbose_name = "Solar Satellite"
    base_cost = (0, 2000, 500)
    prerequisites = (
        ("shipyard", 1),
        )

    def _touch(self, queue_item, new_items, now):
        Ship._touch(self, queue_item, new_items, now)
        queue_item.parent.planet.resources.update_resources(now)

    def production(self):
        avg_temp = (self.planet.maximum_temperature + self.planet.minimum_temperature) / 2
        return int(float(avg_temp + 160) / 6)

# name -> Ship
ShipTypes = dict((klass.name, klass) for klass in [
    LightFighter, HeavyFighter, Cruiser, Bomber,
    Battleship, Battlecruiser, Destroyer, DeathStar,
    SmallCargoShip, LargeCargoShip, Recycler,
    ColonyShip, EspionageProbe, SolarSatellite,
    ])

class Ships(models.Model):
    """Ship counts."""
    class Meta(object):
        verbose_name_plural = "Ships"

    owned_objects = ShipTypes

    planet = models.OneToOneField(core.Planet, primary_key=True)

    small_cargo_ship_count = ZeroField()
    large_cargo_ship_count = ZeroField()
    light_fighter_count = ZeroField()
    heavy_fighter_count = ZeroField()
    cruiser_count = ZeroField()
    battleship_count = ZeroField()
    battlecruiser_count = ZeroField()
    destroyer_count = ZeroField()
    death_star_count = ZeroField()
    bomber_count = ZeroField()
    recycler_count = ZeroField()
    espionage_probe_count = ZeroField()
    solar_satellite_count = ZeroField()
    colony_ship_count = ZeroField()

    def __unicode__(self):
        return "Ships on %s" % self.planet

    def get_class(self, name):
        return ShipTypes[name]

    def get_object(self, name):
        return ShipTypes[name](self.planet)

    def count(self, name):
        return getattr(self, name + "_count")

    def set_count(self, name, count):
        setattr(self, name + "_count", count)
