# -*- coding: utf-8 -*-

import datetime
from sgame import core, queues

"""Base class for ships and defenses."""

class ShipyardObject(core.ObjectBase):
    base_cost = None

    def __init__(self, planet):
        if planet is not None:
            self.planet = planet

    def build_cost(self, count=1):
        """Calculate the cost of building a certain number of this ship."""
        return (self.base_cost[0] * count, # Metal
                self.base_cost[1] * count, # Crystal
                self.base_cost[2] * count) # Deuterium

    def build_time(self, count=1, when=None):
        """Calculate how long building a certain number of this ship will take."""
        sy_level = self.planet.buildings.level("shipyard", False, when)
        nf_level = self.planet.buildings.level("nanite_factory", False, when)

        metal_cost, crystal_cost, deuterium_cost = self.build_cost(count)
        hours = (float(metal_cost + crystal_cost) /
                 (2500 * (1 + sy_level) * 2 ** nf_level))
        return datetime.timedelta(hours=hours)

    def have_enough_resources(self, count):
        metal_cost, crystal_cost, deuterium_cost = self.build_cost(count)
        return (metal_cost <= self.planet.resources.current_metal() and
                crystal_cost <= self.planet.resources.current_crystal() and
                deuterium_cost <= self.planet.resources.current_deuterium())

    def check_resources(self, queue_item):
        if not self.have_enough_resources(queue_item.count):
            print "Error: Cannot start", queue_item, "due to resource shortage."
            raise queues.CannotStart("Not enough resources")

    def parent_available(self, count, now, when=None):
        if when is None:
            if now:
                when = datetime.datetime.now()
            else:
                when = queues.ShipyardQueue.objects.get_end_time(self.planet)
                if when is None:
                    when = datetime.datetime.now()
        end_time = when + self.build_time(count)
        return not queues.BuildingQueue.objects.is_running_at(
            self.planet, "shipyard", when, end_time)

    def check_parent_available(self, queue_item):
        if not self.parent_available(queue_item.count, True):
            print "Error: Cannot start", queue_item, "due to unavailable parent."
            raise queues.CannotStart("Parent building unavailable.")

    def can_build(self, count=1):
        """Quick test to determine whether this can be queued."""
        if queues.ShipyardQueue.objects.is_empty(self.planet):
            return (self.have_enough_resources(1) and
                    self.requirements_met(self.planet, True) and
                    self.parent_available(count, True))
        else:
            when = queues.ShipyardQueue.objects.get_end_time(self.planet)
            return (self.requirements_met(self.planet, False, when) and
                    self.parent_available(count, False, when))

    def touch(self, queue_item):
        # The job of this function: Whenever the queue is touched, update the
        # number of ships/defenses that have been built so far.

        now = datetime.datetime.now()
        elapsed = min(now - queue_item.parent.start_time,
                      queue_item.parent.end_time - queue_item.parent.start_time)
        elapsed_seconds = (elapsed.days * 86400 + elapsed.seconds +
                           float(elapsed.microseconds) / 1000000)

        # This when parameter should be safe, I think
        time_per_item = self.build_time(1, when=queue_item.parent.start_time)
        seconds_per_item = (time_per_item.days * 86400 + time_per_item.seconds +
                            float(time_per_item.microseconds) / 1000000)

        finished = int(elapsed_seconds // seconds_per_item)
        new_items = finished - queue_item.finished # Since last queue checkup
        queue_item.finished = finished
        queue_item.save()

        self._touch(queue_item, new_items, now)
