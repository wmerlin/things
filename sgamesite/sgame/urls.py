# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *

urlpatterns = patterns("sgame.views",
    (r"^$", "index"),
    (r"^planet/(?P<planet_id>\d+)/$", "planet_overview"),
    (r"^planet/(?P<planet_id>\d+)/resources/$", "planet_resources"),
    (r"^planet/(?P<planet_id>\d+)/facilities/$", "planet_facilities"),
    (r"^planet/(?P<planet_id>\d+)/research/$", "planet_research"),
    (r"^planet/(?P<planet_id>\d+)/shipyard/$", "planet_shipyard"),
    (r"^planet/(?P<planet_id>\d+)/defense/$", "planet_defense"),
)
