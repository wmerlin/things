# -*- coding: utf-8 -*-

import datetime
from django import forms, http
from django.core import urlresolvers
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect
import jinja2
from sgame import defenses, models, buildings, research, ships

## Jinja2 setup

def reverse_url(view_name, *args, **kwargs):
    # Copied from the internet, copied from the internet.
    try:
        return urlresolvers.reverse(view_name, args=args, kwargs=kwargs)
    except urlresolvers.NoReverseMatch:
        try:
            project_name = settings.SETTINGS_MODULE.split(".")[0]
            return urlresolvers.reverse(
                project_name + "." + view_name, args=args, kwargs=kwargs)
        except urlresolvers.NoReverseMatch:
            return ""

def media_url(path):
    return settings.SGAME_MEDIA_URL + path

jinja2_env = jinja2.Environment(loader=jinja2.PackageLoader("sgame"), trim_blocks=True)
jinja2_env.globals["url"] = reverse_url
jinja2_env.globals["media"] = media_url

## Template Utilities

def render_template(name, context):
    template = jinja2_env.get_template(name)
    return http.HttpResponse(template.render(context))

def render_sgame_template(user, planet_list, planet, template_name, context={}):
    building_queue = (models.BuildingQueue.objects.filter(parent__planet=planet).
                      order_by("parent__start_time"))
    research_queue = (models.ResearchQueue.objects.filter(parent__user=user).
                      order_by("parent__start_time"))
    shipyard_queue = (models.ShipyardQueue.objects.filter(parent__user=user).
                      order_by("parent__start_time"))

    return render_template(template_name, dict(context, **{
        "user": user,
        "planet_list": planet_list,
        "planet": planet,

        "resources": planet.resources,
        "buildings": planet.buildings,
        "technologies": user.technologies,
        "ships": planet.ships,
        "defenses": planet.defenses,

        "building_queue": building_queue,
        "research_queue": research_queue,
        "shipyard_queue": shipyard_queue,
        }))

def preamble(request, planet_id):
    user = request.user.get_profile()
    models.Queue.objects.process_queue(user)
    planet_list = get_planet_list(user)
    planet = get_planet(user, planet_id)
    return user, planet_list, planet

def generic_view(template_name):
    @login_required
    def view(request, planet_id):
        user, planet_list, planet = preamble(request, planet_id)
        return render_sgame_template(user, planet_list, planet, template_name)
    return view

## Other utilities

def get_planet_list(user):
    return models.Planet.objects.filter(user=user)

def get_planet(user, planet_id):
    # Hm, this might be considered "bad"- get_planet() is called in nearly
    # every method and we use a 404 as a security measure so you can't
    # manipulate other people's planets. Oh well.
    return get_object_or_404(models.Planet, user=user, id=planet_id)

## Views

@login_required
def index(request):
    # Todo: The notion of a home planet is a little silly, but we have to start
    # somewhere. This option should be configurable, however.
    planet_list = get_planet_list(request.user.get_profile())
    home_planet = planet_list.order_by("id")[0]
    return redirect("sgame.views.planet_overview", planet_id=home_planet.id)

planet_overview = generic_view("planet-overview.html")

class BuildForm(forms.Form):
    name = forms.CharField()

class ResearchForm(forms.Form):
    name = forms.CharField()

class ShipyardForm(forms.Form):
    name = forms.CharField()
    count = forms.IntegerField(min_value=1)

def building_view(template_name, redirect_to):
    @login_required
    def view(request, planet_id):
        user, planet_list, planet = preamble(request, planet_id)
        if request.method == "POST":
            def cannot_build(error_msg):
                return render_sgame_template(
                    user, planet_list, planet, template_name, {
                        "error_message": error_msg,
                        })

            form = BuildForm(request.POST)
            if not form.is_valid():
                return cannot_build("Invalid form.")

            building_name = form.cleaned_data["name"]
            if building_name not in buildings.BuildingTypes:
                return cannot_build("Invalid building.")
            building = planet.buildings.get_object(building_name)

            building_queue = models.BuildingQueue.objects.order_by("-parent__end_time")
            if building_queue:
                now = False
                start_time = building_queue[0].parent.end_time
            else:
                if not building.have_enough_resources():
                    return cannot_build("You do not have enough resources available.")

                now = True
                start_time = datetime.datetime.now()

            if not building.requirements_met(planet,now):
                # A better message (what's missing?) would be nice.
                return cannot_build("The requirements for this building are not met.")
            if building.is_in_use(now):
                # Again- what's blocking it?
                return cannot_build("This building is currently in use.")

            target_level = building.level(False) + 1

            metal_cost, crystal_cost, deuterium_cost = building.build_cost(target_level)
            # Annoyingly enough, this recalculates build cost...
            end_time = start_time + building.build_time(target_level)

            queue_item = models.Queue(
                user=user, planet=planet, start_time=start_time, end_time=end_time,
                subqueue_name="building")
            queue_item.save()
            building_queue_item = models.BuildingQueue(
                parent=queue_item, target_level=target_level, name=building_name,
                metal_cost=metal_cost, crystal_cost=crystal_cost, deuterium_cost=deuterium_cost)
            building_queue_item.save()

            return redirect(redirect_to, planet_id=planet_id)

        return render_sgame_template(user, planet_list, planet, template_name)
    return view

planet_resources = building_view("planet-resources.html", "sgame.views.planet_resources")
planet_facilities = building_view("planet-facilities.html", "sgame.views.planet_facilities")

@login_required
def planet_research(request, planet_id):
    user, planet_list, planet = preamble(request, planet_id)
    if request.method == "POST":
        def cannot_research(error_msg):
            return render_sgame_template(
                user, planet_list, planet, "planet-research.html", {
                    "error_message": error_msg,
                    })

        form = ResearchForm(request.POST)
        if not form.is_valid():
            return cannot_build("Invalid form.")

        technology_name = form.cleaned_data["name"]
        if technology_name not in research.TechnologyTypes:
            return cannot_build("Invalid technology.")
        technology = user.technologies.get_technology(technology_name)

        technology_queue = models.ResearchQueue.objects.order_by("-parent__end_time")
        if technology_queue:
            now = False
            start_time = technology_queue[0].parent.end_time
        else:
            if not technology.have_enough_resources(planet):
                return cannot_research("You do not have enough resources available.")

            now = True
            start_time = datetime.datetime.now()

        if not technology.requirements_met(planet, now):
            # A better message (what's missing?) would be nice.
            return cannot_research("The requirements for this technology are not met.")
        if not technology.parent_available(planet, now):
            return cannot_research("The research lab is not available.")

        target_level = technology.level(False) + 1

        metal_cost, crystal_cost, deuterium_cost = technology.research_cost(target_level)
        # Annoyingly enough, this recalculates build cost...
        end_time = start_time + technology.research_time(planet, target_level)

        queue_item = models.Queue(
            user=user, planet=planet, start_time=start_time, end_time=end_time,
            subqueue_name="research")
        queue_item.save()
        technology_queue_item = models.ResearchQueue(
            parent=queue_item, target_level=target_level, name=technology_name,
            metal_cost=metal_cost, crystal_cost=crystal_cost, deuterium_cost=deuterium_cost)
        technology_queue_item.save()

        return redirect("sgame.views.planet_research", planet_id)

    return render_sgame_template(user, planet_list, planet, "planet-research.html")

def shipyard_view(template_name, redirect_to, types):
    @login_required
    def view(request, planet_id):
        user, planet_list, planet = preamble(request, planet_id)
        if request.method == "POST":
            def cannot_build(error_msg):
                return render_sgame_template(
                    user, planet_list, planet, template_name, {
                        "error_message": error_msg,
                        "form": form,
                        })

            form = ShipyardForm(request.POST)
            if not form.is_valid():
                return cannot_build("Invalid form.")

            name = form.cleaned_data["name"]
            if name not in types:
                return cannot_build("Invalid ship/defense.") # Ew
            item = planet.get_object(name)
            count = form.cleaned_data["count"]

            shipyard_queue = models.ShipyardQueue.objects.order_by("-parent__end_time")
            if shipyard_queue:
                now = False
                start_time = shipyard_queue[0].parent.end_time
            else:
                if not item.have_enough_resources(count):
                    return cannot_build("You do not have enough resources available.")

                now = True
                start_time = datetime.datetime.now()

            if not item.requirements_met(planet, now):
                # A better message (what's missing?) would be nice.
                return cannot_build("The requirements for this ship/defense are not met.")
            if not item.parent_available(count, now):
                return cannot_build("The shipyard is not available.")

            metal_cost, crystal_cost, deuterium_cost = item.build_cost(count)
            # Annoyingly enough, this recalculates build cost...
            end_time = start_time + item.build_time(count)

            queue_item = models.Queue(
                user=user, planet=planet, start_time=start_time, end_time=end_time,
                subqueue_name="shipyard")
            queue_item.save()
            shipyard_queue_item = models.ShipyardQueue(
                parent=queue_item, count=count, name=name,
                metal_cost=metal_cost, crystal_cost=crystal_cost, deuterium_cost=deuterium_cost)
            shipyard_queue_item.save()

            return redirect(redirect_to, planet_id)
        else:
            form = ShipyardForm()

        return render_sgame_template(user, planet_list, planet, template_name, {
            "form": form,
            })
    return view

planet_shipyard = shipyard_view("planet-shipyard.html", "sgame.views.planet_shipyard", ships.ShipTypes)
planet_defense = shipyard_view("planet-defense.html", "sgame.views.planet_defense", defenses.DefenseTypes)
