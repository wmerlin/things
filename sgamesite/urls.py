# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns("",
    # Uncomment the admin/doc line below and add "django.contrib.admindocs"
    # to INSTALLED_APPS to enable admin documentation:
    # (r"^admin/doc/", include("django.contrib.admindocs.urls")),

    (r"^admin/", include(admin.site.urls)),
    (r"^sgame/", include("sgame.urls")),
    (r"^accounts/", include("registration.urls")),
    # This should only be reached by the development server.
    (r"^sgame/static/(?P<path>.*)$", "django.views.static.serve",
        {"document_root": settings.SGAME_MEDIA_ROOT}),
)
